-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 192.168.3.252    Database: dboard
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applogs`
--

DROP TABLE IF EXISTS `applogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) DEFAULT NULL,
  `IP` varchar(255) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applogs`
--

LOCK TABLES `applogs` WRITE;
/*!40000 ALTER TABLE `applogs` DISABLE KEYS */;
INSERT INTO `applogs` VALUES (1,'Login','192.168.3.15','Joene Floresca','2015-08-20 12:20:06','2015-08-20 12:20:06');
/*!40000 ALTER TABLE `applogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CampaignName` varchar(255) DEFAULT NULL,
  `Status` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns`
--

LOCK TABLES `campaigns` WRITE;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
INSERT INTO `campaigns` VALUES (1,'AAH',NULL,NULL,'2015-06-23 07:09:12'),(2,'ACTIONFORBLIND',NULL,NULL,NULL),(3,'ADMIN FEE',NULL,NULL,NULL),(4,'ALZHEIMERâ€™S SOCIETY ',NULL,NULL,NULL),(5,'AMNESTY',NULL,NULL,NULL),(6,'AMR',NULL,NULL,NULL),(7,'ANIMAL CHARITIES',NULL,NULL,NULL),(8,'ARUK',NULL,NULL,NULL),(9,'ASCOT INTERNATIONAL',NULL,NULL,NULL),(10,'ASK ',NULL,NULL,NULL),(11,'ASK RAFFLE CALLING',NULL,NULL,NULL),(12,'ASK RAFFLE DATA',NULL,NULL,NULL),(13,'ASSISTANCE DOGS AUSTRALIA',NULL,NULL,NULL),(14,'ATRIUM LEGAL ',NULL,NULL,NULL),(15,'AVACADE',NULL,NULL,NULL),(16,'AZURE',NULL,NULL,NULL),(17,'B&Q',NULL,NULL,NULL),(18,'BACK DATA',NULL,NULL,NULL),(19,'BARNADOS',NULL,NULL,NULL),(20,'BATTERSEA DOGS AND CATS',NULL,NULL,NULL),(21,'BCC',NULL,NULL,NULL),(22,'BCC MCS',NULL,NULL,NULL),(23,'BCC MCS EXPRESS',NULL,NULL,NULL),(24,'BCC TPS',NULL,NULL,NULL),(25,'BLINDS',NULL,NULL,NULL),(26,'BLISS',NULL,NULL,NULL),(27,'BOILER',NULL,NULL,NULL),(28,'BOILER INSURANCE',NULL,NULL,NULL),(29,'BOLIER INSURANCE',NULL,NULL,NULL),(30,'BREAST CANCER',NULL,NULL,NULL),(31,'BROADBAND COMPARISON ',NULL,NULL,NULL),(32,'BROADBAND PROVIDER',NULL,NULL,NULL),(33,'BT LANDLINE',NULL,NULL,NULL),(34,'BTBC',NULL,NULL,NULL),(35,'CAMPHILL VILLAGE TRUST',NULL,NULL,NULL),(36,'CANCER CHARITY',NULL,NULL,NULL),(37,'CANTEEN',NULL,NULL,NULL),(38,'CATS PROTECTION',NULL,NULL,NULL),(39,'CAVITY INSULATION',NULL,NULL,NULL),(40,'CHARITY',NULL,NULL,NULL),(41,'CHARITY LOTTERY',NULL,NULL,NULL),(42,'CHILDFUND',NULL,NULL,NULL),(43,'CHILDFUND NZ',NULL,NULL,NULL),(44,'CHILDREN CHARITY',NULL,NULL,NULL),(45,'CHILDREN WITH CANCER DIRECT',NULL,NULL,NULL),(46,'CLAIMS',NULL,NULL,NULL),(47,'CLC',NULL,NULL,NULL),(48,'CLUB LA COSTA',NULL,NULL,NULL),(49,'CONSERVATION FOUNDATION',NULL,NULL,NULL),(50,'CONSERVATORY',NULL,NULL,NULL),(51,'CONSEVATORY',NULL,NULL,NULL),(52,'CONSUMERLIFEINSURANCE',NULL,NULL,NULL),(53,'CREDIT CARDS ',NULL,NULL,NULL),(54,'CRISIS',NULL,NULL,NULL),(55,'CRUK',NULL,NULL,NULL),(56,'CUSTOMER SERVICE',NULL,NULL,NULL),(57,'DATA ',NULL,NULL,NULL),(58,'DEBT MANAGEMENT ',NULL,NULL,NULL),(59,'DEBT REDUCTION',NULL,NULL,NULL),(60,'DIABETES',NULL,NULL,NULL),(61,'DIRECT DEBIT',NULL,NULL,NULL),(62,'DIRECT DEBIT FOC',NULL,NULL,NULL),(63,'DIRECTDEBIT',NULL,NULL,NULL),(64,'DIRECTDEBIT GENERIC ',NULL,NULL,NULL),(65,'DOGTRUST',NULL,NULL,NULL),(66,'DONKEY SANCTUARY',NULL,NULL,NULL),(67,'EDF',NULL,NULL,NULL),(68,'ELDERLY CHARITY',NULL,NULL,NULL),(69,'EMCAS ISA',NULL,NULL,NULL),(70,'EMCAS PPI',NULL,NULL,NULL),(71,'EMERGENCY AND DISASTER RELIEF CHARITY',NULL,NULL,NULL),(72,'ENDOWMENT',NULL,NULL,NULL),(73,'ENDOWMENT MORTGAGE',NULL,NULL,NULL),(74,'ENERGY',NULL,NULL,NULL),(75,'ENERYGY',NULL,NULL,NULL),(76,'ENVIRONMENTAL CAUSES',NULL,NULL,NULL),(77,'EON',NULL,NULL,NULL),(78,'EQUITY PLAN',NULL,NULL,NULL),(79,'EXCESSIVE NOISE',NULL,NULL,NULL),(80,'EXPRESS',NULL,NULL,NULL),(81,'FLIGHT DELAYS',NULL,NULL,NULL),(82,'FOOTBALL POOLS',NULL,NULL,NULL),(83,'FRIENDS OF EARTH',NULL,NULL,NULL),(84,'FUEL',NULL,NULL,NULL),(85,'FUEL VOUCHER',NULL,NULL,NULL),(86,'FUNDRAISING',NULL,NULL,NULL),(87,'FUNERAL',NULL,NULL,NULL),(88,'FUNERAL LL',NULL,NULL,NULL),(89,'FUNERAL PLAN',NULL,NULL,NULL),(90,'GMG_AMNESTY INTERNATIONAL',NULL,NULL,NULL),(91,'GMG_ASSISTANCE DOGS AUSTRALIA',NULL,NULL,NULL),(92,'GMG_OXFAM AU',NULL,NULL,NULL),(93,'GOLDEN CHARTER',NULL,NULL,NULL),(94,'GOSH',NULL,NULL,NULL),(95,'GOSH CHILDRENS CHARITY',NULL,NULL,NULL),(96,'GPT DATA',NULL,NULL,NULL),(97,'GPT HL',NULL,NULL,NULL),(98,'GPT LOTTERY',NULL,NULL,NULL),(99,'GPT RAFFLE CALLING',NULL,NULL,NULL),(100,'GPT RAFFLE DATA',NULL,NULL,NULL),(101,'GPT RAFFLECALLING',NULL,NULL,NULL),(102,'GPT WL FOC',NULL,NULL,NULL),(103,'GREENPEACE',NULL,NULL,NULL),(104,'GUIDEDOGS',NULL,NULL,NULL),(105,'GUIDEDOGS FOR THE BLIND',NULL,NULL,NULL),(106,'HEALTH AND RESEARCH',NULL,NULL,NULL),(107,'HEALTH AND RESEARCH  CHARITY',NULL,NULL,NULL),(108,'HEARING',NULL,NULL,NULL),(109,'HEART FOUNDATION',NULL,NULL,NULL),(110,'HERITAGE WILLS',NULL,NULL,NULL),(111,'HL',NULL,NULL,NULL),(112,'HOLIDAY TOUR',NULL,NULL,NULL),(113,'HOLIDAY-CLC',NULL,NULL,NULL),(114,'HOME IMPROVEMENT',NULL,NULL,NULL),(115,'HOME INSURANCE',NULL,NULL,NULL),(116,'HOME OWNERS',NULL,NULL,NULL),(117,'HOME PHONE & BROADBAND',NULL,NULL,NULL),(118,'HOMEOWNERS',NULL,NULL,NULL),(119,'HOMEOWNERS - BACKDATA',NULL,NULL,NULL),(120,'HRI HL',NULL,NULL,NULL),(121,'HRI LOTTERY',NULL,NULL,NULL),(122,'HRI NZ',NULL,NULL,NULL),(123,'HRI RAFFLE CALLING',NULL,NULL,NULL),(124,'HRI RAFFLE DATA',NULL,NULL,NULL),(125,'HRI UPSELL DATA',NULL,NULL,NULL),(126,'HUMANITARIAN',NULL,NULL,NULL),(127,'ICYNENE',NULL,NULL,NULL),(128,'IFAW MAILING',NULL,NULL,NULL),(129,'INTERNATIONAL AND HUMANITARIAN CHARITY ',NULL,NULL,NULL),(130,'INVESTMENT ',NULL,NULL,NULL),(131,'KRUK',NULL,NULL,NULL),(132,'LAITHWAITES',NULL,NULL,NULL),(133,'LIFE COVER',NULL,NULL,NULL),(134,'LIFE INSURANCE',NULL,NULL,NULL),(135,'LIFELINE',NULL,NULL,NULL),(136,'LIST RENTAL',NULL,NULL,NULL),(137,'LOAN REPAYMENTS',NULL,NULL,NULL),(138,'LOANS',NULL,NULL,NULL),(139,'LOCAL COUNCIL ',NULL,NULL,NULL),(140,'MAGAZINE',NULL,NULL,NULL),(141,'MAILING',NULL,NULL,NULL),(142,'MARIE CURIE',NULL,NULL,NULL),(143,'MEDICAL CONDITION',NULL,NULL,NULL),(144,'MIDLANDS',NULL,NULL,NULL),(145,'MILLINGTON',NULL,NULL,NULL),(146,'MISSION AVIATION FELLOWSHIP',NULL,NULL,NULL),(147,'MOBILE DATA',NULL,NULL,NULL),(148,'MOBILE PROVIDERS',NULL,NULL,NULL),(149,'MOBILITY',NULL,NULL,NULL),(150,'MORTGAGE',NULL,NULL,NULL),(151,'MOVERS',NULL,NULL,NULL),(152,'MSF',NULL,NULL,NULL),(153,'NDCS',NULL,NULL,NULL),(154,'NEWPAPER',NULL,NULL,NULL),(155,'NEWSPAPER',NULL,NULL,NULL),(156,'NEWSPAPER-LETS SUBSCRIBE',NULL,NULL,NULL),(157,'NOISE',NULL,NULL,NULL),(158,'NOISE YES CLAIMS',NULL,NULL,NULL),(159,'NSPCC',NULL,NULL,NULL),(160,'NSPCC 3RD PARTY ',NULL,NULL,NULL),(161,'O2',NULL,NULL,NULL),(162,'OUTSOURCE',NULL,NULL,NULL),(163,'OVEN',NULL,NULL,NULL),(164,'OXFAM',NULL,NULL,NULL),(165,'OXFAM AU',NULL,NULL,NULL),(166,'OXFAM MCS EXPRESS',NULL,NULL,NULL),(167,'PACKAGED BANK ACCOUNT',NULL,NULL,NULL),(168,'PENSION',NULL,NULL,NULL),(169,'PENSION 2477',NULL,NULL,NULL),(170,'PERSONAL INJURY',NULL,NULL,NULL),(171,'PET',NULL,NULL,NULL),(172,'PHOTOSHOOTS',NULL,NULL,NULL),(173,'PLAN',NULL,NULL,NULL),(174,'PMI',NULL,NULL,NULL),(175,'POST TPS',NULL,NULL,NULL),(176,'POSTCODE LOTTERY',NULL,NULL,NULL),(177,'PPI',NULL,NULL,NULL),(178,'PPI 2ND USE',NULL,NULL,NULL),(179,'PROFILE',NULL,NULL,NULL),(180,'PROFILED DATA',NULL,NULL,NULL),(181,'PROPERTY CLAIM',NULL,NULL,NULL),(182,'QUARRIERS',NULL,NULL,NULL),(183,'RAFBF',NULL,NULL,NULL),(184,'RE-MORTGAGE ',NULL,NULL,NULL),(185,'RED CROSS ',NULL,NULL,NULL),(186,'RNIB',NULL,NULL,NULL),(187,'RSPB',NULL,NULL,NULL),(188,'RTA',NULL,NULL,NULL),(189,'SAVINGS AND INVESTMENT',NULL,NULL,NULL),(190,'SCOTTISH',NULL,NULL,NULL),(191,'SCOTTISH POWER',NULL,NULL,NULL),(192,'SEARCH AND RESCUE',NULL,NULL,NULL),(193,'SECRET WORLD WILDLIFE',NULL,NULL,NULL),(194,'SEE ABILLITY',NULL,NULL,NULL),(195,'SEEING EYE DOGS',NULL,NULL,NULL),(196,'SETTLE MY DEBT',NULL,NULL,NULL),(197,'SHELTER AND HOUSING ',NULL,NULL,NULL),(198,'SKY',NULL,NULL,NULL),(199,'SOLAR PANEL',NULL,NULL,NULL),(200,'SPANA',NULL,NULL,NULL),(201,'SUNLIFEDIRECT',NULL,NULL,NULL),(202,'TALK TALK ',NULL,NULL,NULL),(203,'TAX',NULL,NULL,NULL),(204,'TEARFUND',NULL,NULL,NULL),(205,'TELEPHONE',NULL,NULL,NULL),(206,'TEST',NULL,NULL,NULL),(207,'THE EPILEPSY FOUNDATION',NULL,NULL,NULL),(208,'THE OBERSVER / THE GUARDIAN',NULL,NULL,NULL),(209,'THE WEEK/ COMPUTER ACTIVE ',NULL,NULL,NULL),(210,'TRELOAR TRUST',NULL,NULL,NULL),(211,'TV',NULL,NULL,NULL),(212,'TVWARRANTY',NULL,NULL,NULL),(213,'UNICEF',NULL,NULL,NULL),(214,'USWITCH',NULL,NULL,NULL),(215,'UTILITIES',NULL,NULL,NULL),(216,'VAPOURLITES',NULL,NULL,NULL),(217,'VOCATIONAL EDUCATIONAL TRAINING PROGRAM ',NULL,NULL,NULL),(218,'VODAFONE',NULL,NULL,NULL),(219,'WAP',NULL,NULL,NULL),(220,'WASHING MACHINE',NULL,NULL,NULL),(221,'WATER AID',NULL,NULL,NULL),(222,'WEEKLY LOTTERY',NULL,NULL,NULL),(223,'WELL CHILD',NULL,NULL,NULL),(224,'WILL',NULL,NULL,NULL),(225,'WOMANKIND',NULL,NULL,NULL),(226,'WORK ACCIDENT',NULL,NULL,NULL),(227,'WORLD LOTTO',NULL,NULL,NULL),(228,'WSPA',NULL,NULL,NULL),(229,'YES CLAIMS',NULL,NULL,NULL);
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClientName` varchar(255) DEFAULT NULL,
  `TradingName` varchar(255) DEFAULT NULL,
  `RegistrationNumber` varchar(255) DEFAULT NULL,
  `ClientOwner` varchar(255) DEFAULT NULL,
  `ClientCode` varchar(255) DEFAULT NULL,
  `RegisteredAddress` varchar(255) DEFAULT NULL,
  `ClientCountry` varchar(255) DEFAULT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `ContactPerson` varchar(255) DEFAULT NULL,
  `ClientPhoneNumber` varchar(255) DEFAULT NULL,
  `ClientFaxNumber` varchar(255) DEFAULT NULL,
  `ClientWebsite` varchar(255) DEFAULT NULL,
  `SalesContactPerson` varchar(255) DEFAULT NULL,
  `SalesPhoneNumber` varchar(255) DEFAULT NULL,
  `SalesFaxNumber` varchar(255) DEFAULT NULL,
  `SalesEmail` varchar(255) DEFAULT NULL,
  `PaymentContactPerson` varchar(255) DEFAULT NULL,
  `PaymentPhoneNumber` varchar(255) DEFAULT NULL,
  `PaymentFaxNumber` varchar(255) DEFAULT NULL,
  `PaymentEmail` varchar(255) DEFAULT NULL,
  `BankName` varchar(255) DEFAULT NULL,
  `BankAddress` varchar(255) DEFAULT NULL,
  `BankAccountName` varchar(255) DEFAULT NULL,
  `BankAccountNumber` varchar(255) DEFAULT NULL,
  `BankSortCode` varchar(255) DEFAULT NULL,
  `BankIbanNumber` varchar(255) DEFAULT NULL,
  `BankSwiftCode` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'GMG','Graffiti Media Group','','Warren Short','GMG','The Barn, 11 Bury Road Thetford, East Anglia ','UK','IP24 3PJ','Warren Short / Anne Short','1842760075','','www.gmgroup.uk.com  ','','','','','Anne Hunter ','1842760075','1842339501','Anne@gmgroup.uk.com','','','','','','','          ',1,NULL,NULL),(2,'MediaBowl Ltd','MediaBowl Ltd','','Simon Delaney','MB','Globe Works, Penistone Road, Sheffield, S6 3AE','UK',' S6 3AE','Simon Delaney',' 0114 2994062',' 0114 2994064','simon@mediabowl.co.uk','John Rock','0114 2999120','0114 2994064','john@mediabowl.co.uk ','','','','','','','','','','','',1,NULL,NULL),(3,'2evolve','2evolve Pty Ltd','','Tracey Campbell','2EVOLVE','Level 6, 260 Elizabeth Street Sydney NSW','AU','2000','Tracey Campbell','61281149700','61281149701','www.2evolve.com.au','','','','','','','','','','','','','','','          ',1,NULL,NULL),(4,'Thirty One Fundraising','Thirty One Fundraising','8260906','Jonathan Green','THIRTYONE','5th Floor, St. Vedast House, 5-7 St. Vedast Street, Norwich, Norfolk ','UK','NR1 1BT','Jonathan Green','01603 859231','','www.thirtyonegroup.com','Jonathan Green','07738 816 628','','','Jonathan Green','07738 816 628','','JGreen@thirtyonegroup.com','','','','','','','',1,NULL,NULL),(5,'Verve Media',NULL,NULL,'','DQM_VerveMedia','','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(6,'Insight CCI Ltd ','Insight CCI Ltd ','5905371','Melvyn Hill','ICCI','Lawrence House  5 St Andrewâ€™s Hill Norwich Norfolk ','UK','NR2 1AD','James Kay/Melvyn Hill','01603 216198','','','James Kay','','','james.kay@insightfundraising.org','Lisa Taylor and Emma Hayward ','01603 216198','','lisa.taylor@insightfundraising.org, emma.hayward@insightfundraising.org','','','','','','','',1,NULL,NULL),(7,'Greenpeace',NULL,NULL,'','GREENPEACE','','NZ',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(8,'Oxfam Australia',NULL,NULL,'','OXFAM','','AU',NULL,NULL,'yvettep@oxfam.org.au ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(9,'Waysphone','WAYS Phone Pty Ltd','','Martyn Hartley','Ways','Level 1,69 Regent Street,Chippendale','AU','NSW 2008','Martyn Hartley','61280058300','','www.waysphone.com  ','Gavin Baird','','','gavin@waysphone.com','Martyn Hartley/Gavin Baird','61280058300','','martyn@waysphone.com,gavin@waysphone.com','','','','','','','',1,NULL,NULL),(10,'Amnesty International Australia',NULL,NULL,'','Amnesty_International_Australia','L1 79 Myrtle St. Chippendale 2008 Sydney','AU',NULL,NULL,'283967637',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(11,'Test Client',NULL,NULL,'Test Owner','TEST','','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(12,'Childfund NZ','','','Arron Peacock','CF_NZ','Level 6, 17 Albert St PO Box 105630, Auckland','NZ','','Emily Taylor Supporter Acquisition Manager ','6493662279','','','Emily Taylor Supporter Acquisition Manager ','6493662279','','Emily@childfund.org.nz','','','','','','','','','','','',1,NULL,NULL),(13,'Exceed','Exceed Telecom','','Alan Graham ','Exceed','Level 18 Riverside Centre 123 Eagle street Brisbane 4000 Australia','AU','4101','Alan Graham','0424 225 155','1300 559 405','http://exceedtelecom.com.au     ','Bharat Kumar:','0424 225 155','1300 559 405','bharat@exceedtelecom.au','Alan Graham Managing Director','1300 655 056   ','1300 559 405','alan@exceedtelecom.com.au','','','','','','','',1,NULL,NULL),(14,'Bitstacker Limited',NULL,NULL,'Rashmin Patel','Bitstacker','Suite 8 34 Buckingham Palace Road London ','UK',NULL,NULL,'rashmi@hatchster.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(15,'test',NULL,NULL,'dex','testdex','testtest','UK',NULL,NULL,'123333',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(16,'Marketing List Ltd',NULL,NULL,'Paul Chung','Marketing List Ltd','Peter House St Peters Square Oxford Road Manchester MI 5AN ','UK',NULL,NULL,'0161850 5478',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(17,'Insight Australia','Insight Charity Fundraising Services','','Simon Quinn ','Insight_CFS','L 2 Kippax St Surrey Hills,Australia','AU','NSW 2010 ','Simon Quinn ','02 9215 1600','1300 733 121','www.insightcfs.com.au','','','','','','','','','','','','','','','',1,NULL,NULL),(18,'New Media Interactive ','','New Media Interactive ','Robert Hamilton ','New_Media_Interactive','5 Langley Street, London WC2H 9JA Company Reg No: 08045613','UK','','Robert Hamilton ','020 3667 3869','','','','','','','Robert Hamilton Owner and Commercial Director ','2036673869','','robert.hamilton@newmediainteractive.co.uk','','','','','','','',1,NULL,NULL),(19,'Planit',NULL,NULL,'Greg Life','Planit_DQM','3-7 Herbal Hill, London, EC1R 5EJ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(20,'PMDSC LTD old','','','PAUL MIDSON','PMDSC_DQM','35 BURNWAY HORNCHURCH RM11 3SN','UK','','','07770 382238','','','','','','','','','','','','','','','','','',1,NULL,NULL),(21,'The_Data_Explorer',NULL,NULL,'Abhinav Gupta','DEX_DQM','15 Tottenham Lane, London N8 9DJ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(22,'TARGETS_LOCATED ',NULL,NULL,'NEIL DUBBER ','TL_DQM','23 Goodlass Road, Liverpool Merseyside','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(23,'YES_MEDIA_LIMITED ',NULL,NULL,'Paul Elliott ','YM_DQM','Suite 2,Octagon Office, Waterfront, Brighton Marina BNZ 5WB','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(24,'Miranda_Lawson_Financial_Services  ',NULL,NULL,'MIRANDA LAWSON ','MLFS_DQM','30 Sefton Road, Croydon CR0 7HR','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(25,'PINK_LEGAL ',NULL,NULL,'Benjamin Ogden (b.ogden@pink-pin.com)','PINKLEG_DQM','524  Hessle Road Hull North Humberside HU3 5BQ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(26,'THE BIG INTRO LIMITED ',NULL,NULL,'John Doherty john.doherty@reviewanyday.com/','BIL_DQM','Suite 1 Block C, Caerphilly Business Park Caerphilly, CF83 3ED ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(27,'ENERGY ECOSOLUTIONS ',NULL,NULL,'Chris Metcalfe (energyecosolutions@gmail.com)','EECO_DQM','Unit E20 Innovator House SR5 2TD','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(28,'ENERGY EASE LTD ',NULL,NULL,'Alan Hall energyease@gmail.com','EEL_DQM','10 Green Avenue, Canvey Island SS8 0LH','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(29,'AFFORDABLE WARMTH 4 U LTD ',NULL,NULL,'/ Gareth James gareth@affordablewarmth4u.com','AW4U_DQM','81 Lee Lane, Horwich, Bolton, BL6 7AU','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(30,'COVER AND LEGAL ',NULL,NULL,'J. Mychellin jay.mychalkiw@coverandlegal.co.uk','CAL_DQM','Copthall House, King Street, Newcastle ST5 1UE (0800 0209911 / 07983 967485)','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(31,'The Data Partnership ','','','Ben Ramsey -b.ramsey@thedatapartnership.com ','TDP_DQM','Holton Business Park, Holton St Mary Suffolk','UK','','','','','','','','','','','','','','','','','','','','',1,NULL,NULL),(32,'test',NULL,NULL,'','pinkleg_dqm','','AU',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(33,'The Prospect Shop ','The Prospect Shop ','02 91467822','Martin Ruane ','The_Prospect_Shop ','357 Riley St SURRY HILLS ','AU','NSW 2010','Martin Ruane ','02 9281 0299','','www.prospectshop.com.au','','','','','Martin Ruane ','02 9281 0299','','martin@prospectshop.com.au','','','','','','','',1,NULL,NULL),(34,'DataQuest Media','','','Greg Life ','DQM','Suite 1B Bedford Business Centre Bedford','UK','MK40 2PR','Shane Zafe','2036304444','','www.dataquestmedia.co.uk','Greg Life ','2036304444','','sales@dataquestmedia.com.uk','Joanne Valdez','63 (923) 5350938','','joanne.valdez@dataquestmedia.co.uk','','','','','','','',1,NULL,NULL),(35,'Invive',NULL,NULL,'Andrew Phillips andrew.phillips@invive.co.uk','Invive_DQM','Ground Floor, Rear Barn Brookdale Centre Manchester Road  Knutsford Chesire WA16 0SR','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(36,'Purus Technology ;td',NULL,NULL,'Kirsty Attwood','PTLtd_DQM','14 Gateway Mews, London, N11 2UT','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(37,'Accident and Compensation Shop',NULL,NULL,'Sam Kahn','ACS_DQM','408a Bury New Road, Prestwich, Manchester M25 OLD','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(38,'DSC Telemarketing','','','Stuart Cowan ','DSC_DQM','Broadstone Road, Houldsworth Village, Stockport SK5 7DL','UK','','','0161 414 2648','','','','07553 387 603','','stu.cowan@dsc-telemarketing.co.uk  ','Stuart Cowan ','07553 387 603','0161 414 2648','stu.cowan@dsc-telemarketing.co.uk  ','','','','','','','',1,NULL,NULL),(39,'LIS Claims Management Solutions ',NULL,NULL,'William Shaw (wshaw@cmcsolution.co.uk)','CMC_DQM','Heritage Exchange, South Lane, Elland, HX5 0HG. ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(40,'Silver Fox Media Ltd.','','','Matt Jennings ','SFMLtd_DQM','5 Badgers Oak, Bassingham LN59JP','UK','','Matt Jennings ','','','','','','','','','','','','','','','','','','',1,NULL,NULL),(41,'Consumer Care Limited',NULL,NULL,'Wesley Pickthorne','DQM_ConcareWill','St. Andrews House 62 Bridge Street Manchester','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(42,'Business Shapes Ltd',NULL,NULL,'Neil Armstrong','DQM_BSL','161 Alder Road, Poole, Dorset, BH12 4AA','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(43,'DeepBlue (Scotland Limited)',NULL,NULL,'Craig Milton','DQM_DBL','7 Green Street, Strathaven, Lanarkshire','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(44,'ACH Group Management Ltd',NULL,NULL,'Chris Heywood ','DQM_ACH','Imperial Court Exchange Quay Salford Lancashire','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(45,'Diamond Data Solutions Ltd',NULL,NULL,'Clare Cakebread (clare@diamonddatasolutions.co.uk)','DQM_DDS','4, Suite 1, Triq It- Tullier, Attard, Malta, ATD 1523','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(46,'Communication Avenue',NULL,NULL,'Simon Fieldstead','COMMAVE_DQM','1 Trentside Business Village, Farndon Road, Newark ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(47,'Millington Media / The Contact Hub Ltd',NULL,NULL,'Darren Millington (darren@millington.co.uk) ','MIL_DQM','Millington House, Unit 1G, The Point, Bradmarsh Business Park Rotherham, South Yorkshire S60 1BP','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(48,'Open Resolution',NULL,NULL,'(gerald.thomas@openresolution.co.uk)','OR_DQM','Capital Building, Tyndall Street, Cardiff CF10 4AZ','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(49,'Test',NULL,NULL,'test1','test_test','','UK',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),(50,'Test - Company','testing 1','0123123-12312','Mel Dexter Bulan','TEST-D','Makainabang Baliuag Bulacan','UK','3006','Abigael Bulan','9061624784','231546','www.samplewebsite.com','Luzviminda Bulan','9164845157','845975','luzminda_bulan@yahoo.com','Bernadeth Bulan','9174682468','6852474','bulan_bernadeth@yahoo.com','test bank','bank address','account name','123456','test123','321654','1',1,NULL,NULL),(51,'Charwin Private Clients','','','Ranald Mitchell ','Charwin_Private_Clients_DQM','9 Cringleford Chase, Norwich','UK','NR4 7RS','Caidian Johnson','','','','','','','ranald.mitchell@charwin-pc.co.uk','Ranald Mitchell ','','','ranald.mitchell@charwin-pc.co.uk','','','','','','','',1,NULL,NULL),(52,'I-POWER SYSTEMS LTD','I-POWER SYSTEMS LTD','','Steven Johnstone ','IPO_DQM','The Turbine Coach Close Worksop Notts','UK',' S818AP','Steven Johnstone ','','','','Caidian Johnson','','','sales@dataquestmedia.co.uk','','','','','','','','','','','',1,NULL,NULL),(53,'Bootcorp Technologies Pvt Ltd','','','Jyoti Singh','Bootcorp','60/3B Maharaja Tagore Road Kolkata','UK','700031','Jyoti Singh','','','','Jeniffer ','','','jeniffer@bootcorptechnologies.com','','','','','','','','','','','',1,NULL,NULL),(54,'UNITY4 CONTACT CENTRE OUTSOURCING NZ LTD','UNITY4 CONTACT CENTRE OUTSOURCING NZ LTD','','Daniel Turner  CEO ','Unity4','Level 2b 118 Queen Street Auckland','NZ','1010','Daniel Turner','','','','Renee Beck, Luke Hazlett','','','renee.beck@unity4.com, luke.hazlett@unity4.com','Renee Beck, Luke Hazlett','','','renee.beck@unity4.com; luke.hazlett@unity4.com ','','','','','','','',1,NULL,NULL),(55,'Deep Blue (Scotland) Limited','','','craig.milton@deepbluelimited.co.uk','DBL','7 Green Street, Strathaven, Lanarkshire','UK','ML10 6LT','','01357 529 111','','','','','','','','','','','','','','','','','',1,NULL,NULL),(56,'Precision Prospect Leads','','','Adam Spencer','PPL','1 Hampton Place, Brighton, East Sussex','UK','BN1 3DA','adam.spencer@precisionprospectleads.com','','','','Caidian Johnson','','','sales@dataquestmedia.co.uk','','','','','','','','','','','',1,NULL,NULL),(57,'M&E Insulations Ltd & Evo Green Insulations Ltd','','','Mark Sheehy','MEI','Unit 14 Henry Close, Battlefield Enterprise Park, Shrewsbury, Shropshire','UK','SY1 3FE','marksheehy@hotmail.co.uk','','','','Caidian Johnson','','','sales@dataquestmedia.co.uk','Mark Sheehy','','','marksheehy@hotmail.co.uk','','','','','','','',1,NULL,NULL),(58,'Reactiv Media Limited','','','Adam Loveday','RML','The Warehouse, Gas Works Lane, Elland, West Yorkshire','UK','HX5 9HJ','','(0) 7827 340403','','','','','','adam.loveday@reactiv.co.uk','','','','','','','','','','','',1,NULL,NULL),(59,'Millington Media ','','MIL','Darren Millington','UK',') ','','Millington Media ','1BP','Millington House, Unit 1G, The Point, Bradmarsh Business Park Rotherham, South Yorkshire S60 ','Darren Millington','','','Caidian Johnson','','','sales@dataquestmedia.co.uk ','Darren Millington','','','(darren@millington.co.uk) ','','','','','','',1,NULL,NULL),(60,'Quinn Data Facilities, Incorporated','QDF','','Bob Quinn','QDF','415 Cityland 10 Tower I 156 Dela Costa Street Salcedo Village Makati City Philippines','UK','1227','Angelo Gamboa Operations Manager ','+63 2 7531930','','www.qdf-phils.com','','','','','','','','','','','','','','','',1,NULL,NULL),(61,'JumpStart Media Limited','','','David Billington','Jumpstart','Park Business Centre  Wood Lane Birmingham','UK','B24 9QR','David Billington','44 121 288 1801','','www.jumpstartmedia.co.uk','David Billington','44 7970 666697','','david.billington@jumpstartmedia.co.uk','David Billington','44 7970 666697','','david.billington@jumpstartmedia.co.uk','','','','','','','',1,NULL,NULL),(62,'Inhouse','','','','Inhouse','','UK','','','','','','','','','','','','','','','','','','','','',1,NULL,NULL),(63,'Oxfam New Zealand','Oxfam New Zealand','','Rachael Clark Supporter Relations Manager','OxfamNZ','Level 1, 14 West Street, Newton, Auckland ','NZ','','Rachael Clark  ','+64 9 355 6500','','oxfam.org.nz ','Rachael Clark ','+64 9 355 6500','','Rachael.Clark@oxfam.org.nz','','','','','','','','','','','',1,NULL,NULL),(64,'Unity4 Contact Centre Outsourcing','Unity4 ','','MORGAN MCINNES Client Service Director','Unity4','Level 2, 410 Crown St SURRY HILLS NSW 2010','NZ','','MORGAN MCINNES Client Service Director',' +61 2 9699 8279/+61 (0) 422 940 879','+61 2 8307 2499','www.unity4.com','','','','morgan.mcinnes@unity4.com','MORGAN MCINNES Client Service Director','+61 2 9699 8279/+61 (0) 422 940 879','+61 2 8307 2499','morgan.mcinnes@unity4.com','','','','','','','',1,NULL,NULL),(65,'PDV Ltd','PDV Ltd','',' Harbandan Kohli ','PDV','64 Clarendon Road Watford Hertfordshire ','UK','WD17 1DA',' Harbandan Kohli Marketing Assistant','01923 281709','','www.pdvltd.com',' Harbandan Kohli  Marketing Assistant','01923 281709','01923 281 723','Harbandan.Kohli@pdvltd.com','','','','','','','','','','','',1,NULL,NULL),(66,'Save and Raise (Guernsey) Limited','','','David Hogarth â€“ Managing Director','Saveandraise','Port House, 10 Union Street, St Peter Port, Guernsey GY1 2PS','UK','','David Hogarth â€“ Managing Director','','','saveandraise.co.uk','David Hogarth â€“ Managing Director','','','daveh@saveandraise.co.uk','','','','','','','','','','','',1,NULL,NULL),(67,'Targeted Response Direct Limited','Targeted Response Direct Limited','','David Billington','TargetedResponse','Harriet House 118 High Street Birmingham B23 6BG','UK','','','+44 121 288 1801/+44 7970 666697','','www.targetedresponse.co.uk','','','','david@targetedresponse.co.uk','','','','','','','','','','','',1,NULL,NULL),(68,'True Colours Childrenâ€™s Health Trust ',' ','','Cynthia Ward','TCCHT_NZ','PO Box 9458 Hamilton ','NZ','3240','Cynthia Ward, CEO','','','','','','','cynthia@truecolours.org.nz','Cynthia Ward, CEO','','','cynthia@truecolours.org.nz','','','','','','','',1,NULL,NULL),(69,'Lead Performance Ltd','Lead Performance Ltd','7195400','David Fernandes & Troy Attwood','LPL','Monaco House  Unit 1 Monaco Works Station Road Kings Langley Hertfordshire','UK','WD4 8LQ','David Fernandes','1923277932','','','David Fernandes','1923277932','','david@leadperformanceltd.co.uk','','','','','','','','','','','',1,NULL,NULL),(70,'The Collective Agency Ltd','The Collective Agency Ltd','9153063','Michael Newton','CAL','59 Beechwood Gardens, Slough,   Berkshire','UK','SL1 2HP','Michael Newton, Director','7931139583','','www.thecollectiveagencyltd.com','Michael Newton, Director','7931139583','','Michael@thecollectiveagency.co.uk','','','','','','','','','','','',1,NULL,NULL),(71,'Alphatise Ltd','Alphatise Ltd','158 717 796','Paul Pearson','Alphatise','23 Fennell St, North Parramatta, NSW ','AU','2151','Gavin Baird, Data and Telecommunications Manager','+61 2 8324 7521','None','www.alphatise.com','Gavin Baird, Data and Telecommunications Manager','+61 2 8324 7521','None','www.alphatise.com','','','','','','','','','','','',1,NULL,NULL),(72,'The Fundraising Centre','DTS Group','','Justin Scrobogna','DTS Group','Level 1/42 Station Road, Cheltenham, 3192, Victoria, Australia','AU','3192','Justin Scrobogna','(+61) 03 9581 1810','1300 558 133','','Justin Scrobogna','(+61) 03 9581 1810','1300 558 133','justins@dtsgroup.com.au','','','','','','','','','','','',1,NULL,NULL),(73,' TECH ESSENCE LIMITED','','','Stewart Jordan-Tubbs','TEL','Tech Essence Ltd, 10 Fetter Lane, London, EC4A 1BR','UK','EC4A 1BR','Joseph Ward','7817172619','','','Joseph Ward','7817172619','','joe@tech-essence.com','Joseph Ward','7817172619','','joe@tech-essence.com','','','','','','','',1,NULL,NULL),(74,'eSynergy Global Solutions, Inc','eSynergy Global Solutions, Inc','','Tony Fawaz','TM','14th Floor Mutinational bank Corporation 6805Ayala Avenue Makati City','AU','','Rose Estolano / Tony Fawaz','02 552 4664 / 0916 318 2032','','','Rose Estolano / Tony Fawaz','02 552 4664 / 0916 318 2032','','tony@timemachineit.com, rose@timemachineit.com','','','','','','','','','','','',1,NULL,NULL),(75,'Cegura Technology Solutions (P) Ltd.','Cegura Technology Solutions (P) Ltd.','','Mr Gaurav Vimal, Director','Cegura','WEBEL HRDC; P1 Taratala Road; Suite # 14; Taratala Kolkata (India) - 700 088','UK','','Dipendra Pradhan Vice President','+ 91 33 65418133 ','','www.cegura.com','Mr Gaurav Vimal, Director','+91 9062217534 ','','www.cegura.com','','','','','','','','','','','',1,NULL,NULL),(76,'On The Dotted Line Ltd.','On The Dotted Line Ltd.','','Louise Griffiths','Dotted Line','The Third Floor,Langdon House,Langdon Road, Swansea ','UK','SA1 8QY','Louise Griffiths','0161 854 0602','','','Louise Griffiths','8951611','','louise@arabellas.co.uk','Louise Griffiths','','','accounts@onthedottedline.co','','','','','','','',1,NULL,NULL),(77,'The Green Guys Group ','The Green Guys Group','','Ben Henderson ','Green Guys','2/176 Euston Rd, Alexandria','AU','NSW 2015','','1300 511 875','(02) 9557 4441','http://greenguys.com.au/','Ben Henderson ','1300 511 875','(02) 9557 4441','ben@greenguys.com.au','Ben Henderson ','1300 511 875','','ben@greenguys.com.au','','','','','','','',1,NULL,NULL),(78,'The Green Guys Group ','The Green Guys Group','','Ben Henderson ','Green Guys','2/176 Euston Rd, Alexandria','AU','NSW 2015','','1300 511 875','(02) 9557 4441','http://greenguys.com.au/','Ben Henderson ','1300 511 875','(02) 9557 4441','ben@greenguys.com.au','Ben Henderson ','1300 511 875','','ben@greenguys.com.au','','','','','','','',1,NULL,NULL),(79,'The Green Guys Group','The Green Guys Group','','Ben Henderson','Green Guys','2/176 Euston Rd, Alexandria','AU','NSW 2015','Ben Henderson','1300 511 875','(02) 9557 4441','http://greenguys.com.au/','Ben Henderson','1300 511 875','(02) 9557 4441','ben@greenguys.com.au','Ben Henderson','1300 511 875','(02) 9557 4441','ben@greenguys.com.au','','','','','','','',1,NULL,NULL),(80,'The Review Service Ltd','The Review Service','','Leah Masters','Review Service','20-22 WENLOCK ROAD, LONDON, ','UK','N1 7GU','Daniel Grant','7774724188','','','Daniel Grant','','','daniel@thereviewservice.co.uk','Leah Masters','07749 419065','','leah@thereviewservice.co.uk','','','','','','','',1,NULL,NULL),(81,'PMDSC Ltd','PMDSC Ltd','','Paul Midson','PMDSC','30 Station Lane Hornchurch Essex RM12 6NJ','UK','RM12 6NJ','Carla Leonard and Paul Midson','07770 382238 ','','','','','','production@pmdsc.co.uk','Sarah Ward & Sarah Watts','07921 811760 / 07958 709 389 ','','salesledger@pmdsc.co.uk / sarah.watts@pmdsc.co.uk','','','','','','','',1,NULL,NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_orders`
--

DROP TABLE IF EXISTS `customer_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `rate` float DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `first_delivery_date` date DEFAULT NULL,
  `delivery_schedule` varchar(45) DEFAULT NULL,
  `payment_terms` varchar(45) DEFAULT NULL,
  `other_info_link` varchar(300) DEFAULT NULL,
  `suppression_info_link` varchar(300) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_orders`
--

LOCK TABLES `customer_orders` WRITE;
/*!40000 ALTER TABLE `customer_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `ref_number` varchar(255) DEFAULT NULL,
  `order_confirmnation` varchar(255) DEFAULT NULL,
  `po_number` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `end_user` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `po_count` int(11) DEFAULT NULL,
  `oc_count` int(11) DEFAULT NULL,
  `first_delivery` date DEFAULT NULL,
  `delivery_sched` varchar(255) DEFAULT NULL,
  `delivery_method` varchar(255) DEFAULT NULL,
  `file_format` varchar(255) DEFAULT NULL,
  `data_hygiene` varchar(255) DEFAULT NULL,
  `other_instruction` varchar(255) DEFAULT NULL,
  `billing_schedule` varchar(255) DEFAULT NULL,
  `payment_terms` varchar(255) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `development_fee` varchar(255) DEFAULT NULL,
  `comission` float DEFAULT NULL,
  `order_total` float DEFAULT NULL,
  `delivered_by` varchar(255) DEFAULT NULL,
  `remarks` text,
  `total_delivered` int(11) DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_headers`
--

DROP TABLE IF EXISTS `question_headers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_headers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `question_code` varchar(255) DEFAULT NULL,
  `po_price` float DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `mis_status` varchar(255) DEFAULT NULL,
  `qa_status` varchar(255) DEFAULT NULL,
  `prod_status` varchar(255) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `is_homeowner` varchar(255) DEFAULT NULL,
  `covered_areas` varchar(255) DEFAULT NULL,
  `postcode_exclusion` varchar(255) DEFAULT NULL,
  `telephone_type` varchar(255) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_headers`
--

LOCK TABLES `question_headers` WRITE;
/*!40000 ALTER TABLE `question_headers` DISABLE KEYS */;
INSERT INTO `question_headers` VALUES (1,'2015-08-25','Test question code',3000,'1','Updated','Added','Added',50,'No','England','MM,GG,HG','Mobile','Other','2015-08-25 10:01:56','2015-08-25 10:01:56');
/*!40000 ALTER TABLE `question_headers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_items`
--

DROP TABLE IF EXISTS `question_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_headers_id` int(11) DEFAULT NULL,
  `question` text,
  `response` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_items`
--

LOCK TABLES `question_items` WRITE;
/*!40000 ALTER TABLE `question_items` DISABLE KEYS */;
INSERT INTO `question_items` VALUES (1,1,' Content here.. ','Yes','2015-08-25 10:01:56','2015-08-25 10:01:56'),(2,1,' Content here.. ','Yes','2015-08-25 10:01:56','2015-08-25 10:01:56'),(3,1,' Content here.. ','Yes','2015-08-25 10:01:56','2015-08-25 10:01:56');
/*!40000 ALTER TABLE `question_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(255) DEFAULT NULL,
  `CompanyAlias` varchar(255) DEFAULT NULL,
  `CompanyRegistrationNumber` varchar(255) DEFAULT NULL,
  `RegisteredAddress` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `CompanyOwner` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(255) DEFAULT NULL,
  `FaxNumber` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Website` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Test Company INC.','TEST','03124-2658','1227 Makati City Philippines','Philippines','Mel Dexter Bulan','9069666705','123456','dexter.bulan@qdf-phils.com','testwebsite.com.ph','Outsource',1,NULL,NULL),(2,' SAMBORA COMMUNICATIONS INC.','SAMBORA','','2 AVENUE OF THE REPUBLIC, GEORGETOWN, GUYANA','UK','Faizal Khan','-7614','','nkechic@samboracommunications.com','www.samboracommunications.com','Wholesale Partner',1,NULL,NULL),(3,'LIFT Prestige Solutions, Inc.','LIFT','','18B Belvedere Tower, San Miguel Avenue, Ortigas Center, Pasig City','Philippines','Iris Lee','','','iris@liftbpo.com','','Wholesale Partner',1,NULL,NULL),(4,'Global Consumer Leads Ltd','GCL','','Bridge house 2, Bridge Avenue Maidenhead SL6 1RR','UK','Md Kurshid Alam ','1753253474','','bdm@global-leads.co.uk','','Wholesale Partner',1,NULL,NULL),(5,'LIFT Prestige Solutions, Inc.','LIFT','','G/F Discovery Suite, 25 ADB Avenue Ortigas Center Pasig City 1605 ','Philippines','Marion Faye DeLeon','','','Aeon Tiamson <aeon.tiamson@gmail.com>','','Wholesale Partner',1,NULL,NULL),(6,'Data Miners Pvt. Ltd.','DMPL','','B-101 Neerav Co operative Housing Society Limited, 90 Feet Road Asha Nagar, Kandivali East, Mumbai-400101, Maharashtra, India','India','Neeru Singhal','','','neeru.singhal@thedataexplorer.','','Wholesale Partner',1,NULL,NULL),(7,'PHOENIX BPO SOLUTIONS','PHX','9515341','110 Dona Nieves St Angono RZL','Philippines','Marion De Leon','63915-3842366','','aeon.tiamson@gmail.com ','','Wholesale Partner',1,NULL,NULL),(8,'Jumpstart Media Ltd','Jumpstart','','Park Business Centre, Wood Lane, Birmingham, B24 9QR','United Kingdom','David Billington','','','david.billington@jumpstartmedia.co.uk','','Wholesale Partner',1,NULL,NULL),(9,'BPO Telequest, Inc.','TQ','',' 3/f St. Francis IT Centre Araneta St., Bacolod City 6100','Philippines ','Ken Rivera','','','ken.bpotelequest@gmail.com','','Outsource',1,NULL,NULL),(10,'Bootcorp Technologies Pvt Ltd','Bootcorp','','60/3B Maharaja Tagore Road Kolkata 700031','India','Jyoti Singh','91 33 6459 9372','','jeniffer@bootcorptechnologies.com','http://www.bootcorptechnologies.com/home.html','Wholesale Partner',1,NULL,NULL),(11,'Debaj Infotech Private Limited','Debaj ','U72300WB2012PTC189495','T5/2H ,Tiljala Road ,Kolkata-700046, West Bengal, India','INDIA','SHAKIR NAWAZ','+91 8961288835','','shakir.nawaz@debajinfotech.com,anurag.mishra@debajinfotech.com','www.debajinfotech.com','WP',1,NULL,NULL),(12,'AG&T Outsource, Inc.','AG&T','','2nd Flr RLG Bldg Candelaria St. Brgy. Bucana, Ecoland, Davao City 8000','Philippines','Emmanuel V. Salvador Director of Operations','9177016246','','evsalvador@agt.com.ph','www.agt.com.ph','Outsource',1,NULL,NULL),(13,'Yes Media (Solutions) Ltd,','YesMedia','07713268 VAT:-123 6496 14','Suite 2 Octagon Office, Waterfront, Brighton Marina, Brighton, East Sussex, BN2 5BW','UK','Paul Elliott,','01273 956092, 01273 956093','01273 958921','accounts@yesmediasolutions.co.uk,returns@yesmediasolutions.co.uk','','Outsource',1,NULL,NULL),(14,'Millington Media / The Contact Hub Ltd','MIL','','Millington House, Unit 1G, The Point, Bradmarsh Business Park Rotherham, South Yorkshire S60 1BP','UK','Darren Millington','','',' (darren@millington.co.uk) ','','Outsource',1,NULL,NULL),(15,'Targeted Response','Targeted Response','','Harriet House 118 High St Birmingham B23 6BG','UK','David Billington','+44 121 288 1801/+44 7970 666697','','david@targetedresponse.co.uk','www.targetedresponse.co.uk','WP',1,NULL,NULL),(16,'DataQuest Media','DQM','','Suite 1B Bedford Business Centre, Bedford MK40 2PR','UK','Greg Life ','0203 6304444','','greg.life@dataquestmedia.co.uk','','WP',1,NULL,NULL),(17,'MediaBowl Ltd','MediaBowl Ltd','7675128','Globe Works, Penistone Road, Sheffield, S6 3AE','UK','Simon Delaney','0114 2994062','0114 2994064','simon@mediabowl.co.uk','www.mediabowl.co.uk','WP',1,NULL,NULL),(18,'Lead Performance Ltd','LPL','7195400','Monaco House  Unit 1 Monaco Works Station Road Kings Langley Hertfordshire WD4 8LQ','UK','David Fernandes','1923277932','','david@leadperformanceltd.co.uk','','WP',1,NULL,NULL),(19,'The Collective Agency Ltd','CAL','9153063',' 59 Beechwood Gardens, Slough,   Berkshire','SL1 2HP','Michael Newton, Director','7931139583','','Michael@thecollectiveagency.co.uk','','WP',1,NULL,NULL),(20,'Cegura Technology Solutions (P) Ltd.','Cegura','U72300WB2012PTC189360','WEBEL HRDC; P1 Taratala Road; Suite # 14; Taratala Kolkata (India) - 700 088','India','Mr Gaurav Vimal, Director','+91 33 65418133','','gaurav.vimal@cegura.com','','WP',1,NULL,NULL),(21,'Vision Vantage','Vision','','125, N.S. Road Kolkota ','India','Shawn Thompson','9748279711','','shawn@visionvantage.co.in','www.visiontage.co.in','WP',1,NULL,NULL),(22,'Digitalbox Ltd. ','','S','15 Gay Street Bath BA1 2PH','UK ','David Cartwright','1225430091','','david.cartwright@digitalbox.com, richard.feist@digitalbox.com','www.digitalbox.com, www.ActiveYou.co.uk','WP',1,NULL,NULL);
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_level` int(11) NOT NULL DEFAULT '0',
  `logs` int(11) NOT NULL DEFAULT '0',
  `address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Joene Floresca','joenefloresca@gmail.com','$2y$10$mInYIaNnDovQvaE6U1R4Q.paxqdmm/n120uTsvxs4xYUgTqhzGH6K',0,0,'N/A','N/A','N/A','5EAiYJUSVcP7dxuy5msrkAVxiJAtjZUpeDWrMEQ5E2bcitIwmm9lgZuS7hNp','2015-06-07 23:44:45','2015-10-29 05:40:21'),(2,'Juan Dela Cruz','juan@gmail.com','$2y$10$Up.GBR5s3JtTYLlXva2HJep6kGqio9OFCDQAwsdXEH1/D35u9RUJO',0,0,'addr','Company1','PH','voeFqpNWHJpwtYVWt9W8iZsxFbSS7Ccb8h2J6HNg69yxwBg9K9u5q0cWB785','2015-10-29 05:53:57','2015-10-29 05:54:06'),(3,'Sharik Khan','asassdsd@gmail.com','$2y$10$fymq2eSqqfOjYacHEdjIAON0ByiZDNodQAzVy2Jg51e/GEFrGz9Dm',0,0,'aaaaaaaaa','aaaaaaaaa','PH',NULL,'2015-10-29 06:02:55','2015-10-29 06:02:55'),(4,'Krishna Rao','sdsdsd@sdsd.com','$2y$10$k7j0cvHtOqeMlywVIVZzLemMEgOINm/Xc1J0KNGIuV7SLgaHvhYGK',0,0,'aaaaaaaa','aaaaaaaaa','AO',NULL,'2015-10-29 06:04:01','2015-10-29 06:04:01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-29 22:41:19
