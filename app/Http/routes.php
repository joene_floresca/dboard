<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');


/* Deliveries*/
Route::get('deliveries', 'DeliveriesController@index');
Route::post('deliveries', 'DeliveriesController@actionPost');

/* Inhouse Campaigns*/
Route::get('inhouse', 'InhouseCampaignsController@index');
Route::post('inhouse', 'InhouseCampaignsController@actionPost');

/* Delivery Tracker */
Route::get('deliverytracker', 'DeliveryTrackerController@deliveryIndex');
Route::get('api/deliverytracker/sorted', 'DeliveryTrackerController@getDeliveriesSorted');

Route::get('api/deliverytracker/complete', 'DeliveryTrackerController@deliveryIndexApiComplete');
Route::get('api/deliverytracker/pending', 'DeliveryTrackerController@deliveryIndexApiPending');

/* Clients Resource Controller*/
Route::resource('client', 'ClientController');

/* Clients API Calls */
Route::get('api/client/all', 'ClientController@apiGetClients');

/* Suppliers Resource Controller*/
Route::resource('supplier', 'SupplierController');

/* Supplier API Calls */
Route::get('api/supplier/all', 'SupplierController@apiGetSuppliers');

/* Campaigns Resource Controller*/
Route::resource('campaign', 'CampaignController');

/* Locations Resource Controller*/
Route::resource('location', 'LocationController');

/* Locations API Calls */
Route::get('api/location/all', 'LocationController@apiGetLocations');


/* Campaigns API Calls */
Route::get('api/campaign/all', 'CampaignController@apiGetCampaigns');

/* Campaigns Resource Controller*/
Route::resource('question', 'QuestionController');

/* Order Resource Controller*/
Route::resource('order', 'OrderController');

/* Order API Calls */
Route::get('api/orders/deliveries', 'OrderController@apiGetDeliveries');
Route::get('api/orders/all', 'OrderController@apiGetOrders');

/* Order Signatature Routes */
Route::get('sign-accepted/{md5}', 'OrderController@showAcceptedSign');
Route::post('sign-accepted/{md5}', 'OrderController@postAcceptedSign');



/* Question Resource Controller*/
Route::resource('question', 'QuestionController');

Route::get('api/question_h/all', 'QuestionController@apiGetQuestionH');

/* Purchase ORder Resource Controller*/
Route::resource('po', 'PurchaseOrderController');





// Route::get('customer-sample', 'OrderController@showCustomerSample');

/* Customer Order route */
Route::get('customer-order', 'OrderController@showCustomerOrder');
Route::post('customer-order', 'OrderController@postCustomerOrder');

Route::get('get-customer-order', 'OrderController@getCustomerOrder');
Route::get('get-customer-order-admin', 'OrderController@getCustomerOrderAdmin');

Route::get('accept-order/{ordernum}', 'OrderController@acceptCustomerOrder');
Route::get('reject-order/{ordernum}', 'OrderController@rejectCustomerOrder');

Route::get('customer-order/{id}', 'OrderController@showCustomerOrderAdmin');

Route::get('customer-order-list', 'OrderController@showCustomerOrderList');

Route::get('order-list', 'OrderController@getOrderList');






Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
