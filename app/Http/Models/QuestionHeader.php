<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class QuestionHeader extends Model {

	protected $table = 'question_headers';

	public function getQuestionHeaderItems($id)
	{
		$data = DB::connection('mysql')->select("SELECT * FROM question_headers a INNER JOIN question_items b ON a.id = b.question_headers_id WHERE a.id =".$id.";");
		return $data;
	}

}
