<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LastOrderNumber extends Model {

	protected $table = 'last_order_number';

}
