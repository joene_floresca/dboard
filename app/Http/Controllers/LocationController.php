<?php namespace App\Http\Controllers;

use Validator;
use Input;
use Redirect;
use Session;
use View;
use \App\Http\Models\Location;

class LocationController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

     public function index()
    {
        return view('location.index');
    }


    public function apiGetLocations()
    {
        $location = Location::orderBy('id', 'DESC')->get();
        return json_encode($location);
    }


	public function create()
	{
		return view('location.create');
	}

	public function store()
	{
		$rules = array(
            'LocationName'     		 => 'required|unique:Locations',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) 
        {
            return Redirect::to('location/create')->withErrors($validator);
        }
        else
        {
        	$location = new Location;
            $location->LocationName 			= Input::get('LocationName');
            $location->save();
           
            Session::flash('alert-success', 'Form Submitted Successfully.');

            return Redirect::to('location/create');
        }
	}

	
}
