<?php namespace App\Http\Controllers;

use Validator;
use Input;
use Redirect;
use Session;
use View;
use DB;
use App\Http\Models\QuestionHeader;
use App\Http\Models\QuestionItem;

class QuestionController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('question.index');
	}

	public function create()
	{
		$campaign_options = array('' => 'Choose One') + DB::table('campaigns')->lists('CampaignName','id');
		$oc_options= array('' => 'Choose One') + DB::table('orders')->lists('ref_number','id');
		return view('question.create')->with(array('campaign_options' => $campaign_options, 'oc_options' => $oc_options));
	}

	public function store()
	{

		$rules = array(
            // 'CampaignName'  	 => 'required',
            // 'PoPrice'  	 		 => 'required',
            // 'Oc_id'  	         => 'required',
            // 'mis_status'  	     => 'required',
            // 'qa_status'          => 'required',
            // 'prod_status'        => 'required',
            // 'Age'  	             => 'required',
            // 'IsHomeOwner'  	     => 'required',
            // 'CoveredAreas'  	 => 'required',
            // 'PostcodeExclusion'  => 'required',
            // 'Telephone'  	     => 'required',   
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) 
        {
            return Redirect::to('question/create')->withErrors($validator);
        }
        else
        {
        	/* Insert to Question Header and Get last Insert ID */
        	$question_h = new QuestionHeader();
        	$question_h->date = date('Y-m-d');
        	$question_h->question_code = "Test question code";
        	$question_h->po_price = Input::get("PoPrice");
        	$question_h->order_id = Input::get("Oc_id");
        	$question_h->mis_status = Input::get("mis_status");
        	$question_h->qa_status = Input::get("qa_status");
        	$question_h->prod_status = Input::get("prod_status");
        	$question_h->age = Input::get("Age");
        	$question_h->is_homeowner = Input::get("IsHomeOwner");
        	$question_h->covered_areas = Input::get("CoveredAreas");
        	$question_h->postcode_exclusion = Input::get("PostcodeExclusion");
        	$question_h->telephone_type = Input::get("Telephone");
        	$question_h->others = Input::get("Others");
        	if($question_h->save())
        	{
                $scripts = Input::get("script");
                $responses = Input::get("response");
                $count = count($scripts);
                $count = count($responses);

                var_dump($count);
                for($x = 0; $x < $count; $x++)
                {
                    $question_i = new QuestionItem();
                    $question_i->question_headers_id = $question_h->id;
                    $question_i->question = $scripts[$x];
                    $question_i->response = $responses[$x];
                    $question_i->save();
                }
           

        		Session::flash('alert-success', 'Form Submitted Successfully.');
        	}
        	else
        	{
        		Session::flash('alert-danger', 'Form submission failed. Please try again.');
        	}


            return Redirect::to('question');
        }

	}

	public function edit($id)
	{
		
	}

	public function update($id)
	{
		
	}

	public function destroy($id)
    {
        
        
    }

    public function show($id)
    {
        $data = new QuestionHeader();
        $headerItems = $data->getQuestionHeaderItems($id);
        $headerData = QuestionHeader::find($id);

        return view('question.show')->with(array('headerItems'=>$headerItems, 'headerData' => $headerData));
    }

    public function apiGetQuestionH()
    {
        return json_encode(QuestionHeader::all());
    }

	

}
