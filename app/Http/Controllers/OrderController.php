<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Validator;
use Input;
use Redirect;
use App\Http\Models\Order;
use App\Http\Models\CustomerOrder;
use App\Http\Models\LastOrderNumber;
use Session;
use View;
use URL;
use Auth;
use App\User;
use Mail;
use Datatables;

class OrderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('order.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$client_options = array('' => 'Choose One') + DB::table('clients')->where('status', '1')->lists('ClientName','ClientCode');
		$campaign_options = array('' => 'Choose One') + DB::table('campaigns')->lists('CampaignName','CampaignName');
		return view('order.create')->with(array('client_options' => $client_options, 'campaign_options' => $campaign_options));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'orderDate'                   => 'required',
            'orderClient'                 => 'required',
            'orderCountry'                => 'required',
            'orderCurrency'               => 'required',
            'orderPoDate'                 => 'required',
            'orderEndUser'                => 'required',
            'orderCategory'               => 'required',
            'orderType'                   => 'required',
            'orderAmount'                 => 'required',
            'orderPoCount'                => 'required',
            'orderOcCount'                => 'required',
            'orderFirstDeliveryDate'      => 'required',
            'orderDeliverySched'          => 'required',
            'orderMainDeliverySched'      => 'required',
            'orderDeliveryMethod'         => 'required',
            'orderFileFormat'             => 'required',
            'orderDataHygiene'            => 'required',
            'orderDeliveryInstruction'    => 'required',
            'orderBillingSched'           => 'required',
            'orderPaymentTerms'           => 'required',
            'orderSubTotal'               => 'required',
            'orderDevelopmentFee'         => 'required',
            'orderCommission'             => 'required',
            'orderTotalValue'             => 'required',
            'orderDeliveredBy'            => 'required',
        );

		$validator = Validator::make(Input::all(), $rules);

        // Check if all fields is filled
        if ($validator->fails())
        {
            return Redirect::to('order/create')->withInput()->withErrors($validator);
        }
        else
        {
        	$order = new Order();
        	$order->date = Input::get("orderDate");
        	$order->client = Input::get("orderClient");
        	$order->country = Input::get("orderCountry");
        	$order->currency = Input::get("orderCurrency");
        	$order->ref_number = Input::get("orderOcNumber").Input::get("orderNumber");
        	$order->order_confirmnation = Input::get("orderNumber");
        	$order->po_date = Input::get("orderPoDate");
        	$order->end_user = Input::get("orderEndUser");
        	$order->category = Input::get("orderCategory");
        	$order->type = Input::get("orderType");
        	$order->amount = Input::get("orderAmount");
        	$order->po_count = Input::get("orderPoCount");
        	$order->oc_count = Input::get("orderOcCount");
        	$order->first_delivery = Input::get("orderFirstDeliveryDate");
        	$order->delivery_sched = Input::get("orderMainDeliverySched");
        	$order->delivery_method = Input::get("orderDeliveryMethod");
        	$order->file_format = Input::get("orderFileFormat");
        	$order->data_hygiene = Input::get("orderDataHygiene");
        	$order->other_instruction = Input::get("orderDeliveryInstruction");
        	$order->billing_schedule = Input::get("orderBillingSched");
        	$order->payment_terms = Input::get("orderPaymentTerms");
        	$order->sub_total = Input::get("orderSubTotal");
        	$order->development_fee = Input::get("orderDevelopmentFee");
        	$order->comission = Input::get("orderCommission");
        	$order->order_total = Input::get("orderTotalValue");
        	$order->delivered_by = Input::get("orderDeliveredBy");
        	$order->remarks = Input::get("orderRemarks");
        	if($order->save())
        	{
        		// $updateOder = Order::find($order->id);
        		// $updateOder->order_confirmnation = str_pad($order->id, 4, '0', STR_PAD_LEFT);
        		// $updateOder->po_number = str_pad($order->id, 4, '0', STR_PAD_LEFT);
        		// //$updateOder->save();
        		// if($updateOder->save())
        		// {
        		// 	$updateRef = Order::find($order->id);
        		// 	$updateRef->ref_number = $order->ref_number.$updateOder->order_confirmnation;
        		// 	$updateRef->save();
						//
        		// }
        		Session::flash('alert-success', 'Order Confirmation Created Successfully!');
        	}
        	else
        	{
        		Session::flash('alert-danger', 'Error creating Order Confirmation! Please try again.');
        	}

        	return Redirect::to('order/create');
        }

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::find($id);
		return view('order.show')->with(array('order'=>$order));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$client_options = array('' => 'Choose One') + DB::table('clients')->where('status', '1')->lists('ClientName','ClientCode');
		$campaign_options = array('' => 'Choose One') + DB::table('campaigns')->lists('CampaignName','CampaignName');
		$order = Order::find($id);
		return View::make('order.edit')->with(array('order' => $order, 'campaign_options' => $campaign_options, 'client_options' => $client_options));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
            'orderDate'                   => 'required',
            'orderClient'                 => 'required',
            'orderCountry'                => 'required',
            'orderCurrency'               => 'required',
            'orderPoDate'                 => 'required',
            'orderEndUser'                => 'required',
            'orderCategory'               => 'required',
            'orderType'                   => 'required',
            'orderAmount'                 => 'required',
            'orderPoCount'                => 'required',
            'orderOcCount'                => 'required',
            'orderFirstDeliveryDate'      => 'required',
            'orderDeliverySched'          => 'required',
            'orderMainDeliverySched'      => 'required',
            'orderDeliveryMethod'         => 'required',
            'orderFileFormat'             => 'required',
            'orderDataHygiene'            => 'required',
            'orderDeliveryInstruction'    => 'required',
            'orderBillingSched'           => 'required',
            'orderPaymentTerms'           => 'required',
            'orderSubTotal'               => 'required',
            'orderDevelopmentFee'         => 'required',
            'orderCommission'             => 'required',
            'orderTotalValue'             => 'required',
            'orderDeliveredBy'            => 'required',
        );

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
        {
            return Redirect::to('order/create')->withInput()->withErrors($validator);
        }
        else
        {
        	$order = Order::find($id);
        	$order->date = Input::get("orderDate");
        	$order->client = Input::get("orderClient");
        	$order->country = Input::get("orderCountry");
        	$order->currency = Input::get("orderCurrency");
        	$order->ref_number = Input::get("orderOcNumber");
        	$order->po_date = Input::get("orderPoDate");
        	$order->end_user = Input::get("orderEndUser");
        	$order->category = Input::get("orderCategory");
        	$order->type = Input::get("orderType");
        	$order->amount = Input::get("orderAmount");
        	$order->po_count = Input::get("orderPoCount");
        	$order->oc_count = Input::get("orderOcCount");
        	$order->first_delivery = Input::get("orderFirstDeliveryDate");
        	$order->delivery_sched = Input::get("orderMainDeliverySched");
        	$order->delivery_method = Input::get("orderDeliveryMethod");
        	$order->file_format = Input::get("orderFileFormat");
        	$order->data_hygiene = Input::get("orderDataHygiene");
        	$order->other_instruction = Input::get("orderDeliveryInstruction");
        	$order->billing_schedule = Input::get("orderBillingSched");
        	$order->payment_terms = Input::get("orderPaymentTerms");
        	$order->sub_total = Input::get("orderSubTotal");
        	$order->development_fee = Input::get("orderDevelopmentFee");
        	$order->comission = Input::get("orderCommission");
        	$order->order_total = Input::get("orderTotalValue");
        	$order->delivered_by = Input::get("orderDeliveredBy");
        	$order->remarks = Input::get("orderRemarks");
        	if($order->save())
        	{
        		Session::flash('alert-success', 'Order Updated Successfully.');
        	}
        	else
        	{
        		Session::flash('alert-danger', 'Error Updating Order Confirmation! Please try again.');
        	}

        	return Redirect::to('order')->withInput()->withErrors($validator);

        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function apiGetDeliveries()
	{
		$now = date('Y-m-d');
		$today = date("l");

		$deliveries = Order::where('first_delivery', '<=', $now)->get();;
		$data = array();
        $delivery_status = "";

		foreach ($deliveries as $key => $value)
		{
			$sched = explode(',',$value->delivery_sched);

			if(in_array($today, $sched) || $value->delivery_sched == "Daily") // Check if the schedule macthes today
			{

				//Push records to $data array for dashboard delivery data
				$data[$key]['id'] 									 = $value->id;
				$data[$key]['date'] 				 				 = $value->date;
				$data[$key]['client']                = $value->client;
				$data[$key]['ref_number']            = $value->ref_number;
				$data[$key]['order_confirmnation']   = $value->order_confirmnation;
				$data[$key]['po_number']             = $value->po_number;
				$data[$key]['po_date']               = $value->po_date;
				$data[$key]['category']              = $value->category;
				$data[$key]['type']                  = $value->type;
				$data[$key]['amount']                = $value->amount;
				$data[$key]['po_count']              = $value->po_count;
				$data[$key]['oc_count']              = $value->oc_count;
				$data[$key]['first_delivery']        = $value->first_delivery;
				$data[$key]['delivery_sched']        = $value->delivery_sched;
				$data[$key]['delivery_method']       = $value->delivery_method;
				$data[$key]['file_format']           = $value->file_format;
				$data[$key]['data_hygiene']          = $value->data_hygiene;
				$data[$key]['other_instruction']     = $value->other_instruction;
				$data[$key]['billing_schedule']      = $value->billing_schedule;
				$data[$key]['payment_terms']         = $value->payment_terms;
				$data[$key]['sub_total']             = $value->sub_total;
				$data[$key]['development_fee']       = $value->development_fee;
				$data[$key]['comission']             = $value->comission;
				$data[$key]['order_total']           = $value->order_total;
				$data[$key]['delivered_by']          = $value->delivered_by;
				$data[$key]['remarks']               = $value->remarks;
				$data[$key]['created_at']            = $value->created_at;
				$data[$key]['updated_at']            = $value->updated_at;
				$data[$key]['total_delivered']       = $value->total_delivered;
			}

		}

		return json_encode($data);
	}

	public function apiGetOrders()
	{
		return json_encode(Order::all());
	}

    public function showCustomerOrder()
    {

        $client_options = array('' => 'Choose One') + DB::table('clients')->where('status', '1')->lists('ClientName','ClientCode');
        $campaign_options = array('' => 'Choose One') + DB::table('campaigns')->lists('CampaignName','CampaignName');
        return view('order.customer-order')->with(array('client_options' => $client_options, 'campaign_options' => $campaign_options));
    }


     public function postCustomerOrder()
    {
				// var_dump(Input::get("question"));
				// var_dump(Input::get("rate"));
				// var_dump(Input::get("orderMainDeliverySched")); exit();

        $rules = array(
            'question'                => 'required',
            'rate'                    => 'required',
            'orderCurrency'           => 'required',
            'orderFirstDeliveryDate'  => 'required',
            'orderPaymentTerms'       => 'required',
            'orderPaymentTerms'       => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('customer-order')->withInput()->withErrors($validator);
        }
        else
        {

            // Get Last Order Number
            $lastOrderNumber = LastOrderNumber::all();
            $orNumber = $lastOrderNumber[0]->order_num;

						// Update the last order number, add 1
            $updateOrNumber = LastOrderNumber::find(1);
            $updateOrNumber->order_num = intval($lastOrderNumber[0]->order_num) + 1;
            $updateOrNumber->save();

            $question                = Input::get("question");
            $rate                    = Input::get("rate");
            $orderCurrency           = Input::get("orderCurrency");
            $orderFirstDeliveryDate   = Input::get("orderFirstDeliveryDate");
            $orderMainDeliverySched  = Input::get("orderMainDeliverySched");
            $orderPaymentTerms       = Input::get("orderPaymentTerms");
            $orderRemarks            = Input::get("orderRemarks");

            $CustomerOrder = new CustomerOrder();
            $CustomerOrder->question                = $question;
            $CustomerOrder->rate                    = $rate;
            $CustomerOrder->currency                = $orderCurrency;
            $CustomerOrder->first_delivery_date     = $orderFirstDeliveryDate;
            $CustomerOrder->delivery_schedule       = $orderMainDeliverySched;
            $CustomerOrder->payment_terms           = $orderPaymentTerms;
            $CustomerOrder->remarks                 = $orderRemarks;
            $CustomerOrder->order_number            = $orNumber;

            /* Get uploaded files and uploaded to server*/
            $file              = Input::file('uploadOtherInfo');
            $filename          = $file->getClientOriginalName();
            $destinationPath   = 'uploads/'.$orNumber.'/';
            $uploadSuccess     = Input::file('uploadOtherInfo')->move($destinationPath, $filename);

            $file1             = Input::file('UploadSuppressionFiles');
            $filename1         = $file1->getClientOriginalName();
            $destinationPath2  = 'uploads/'.$orNumber.'/';
            $uploadSuccess     = Input::file('UploadSuppressionFiles')->move($destinationPath2, $filename1);

            $otherinfo_url     = URL::to('/').'/'.$destinationPath.$filename ;
            $suppression_url   = URL::to('/').'/'.$destinationPath2.$filename1;

            $CustomerOrder->other_info_link       = $otherinfo_url;
            $CustomerOrder->suppression_info_link = $suppression_url;
            $CustomerOrder->created_by_id = Auth::user()->id;
            $CustomerOrder->save();


            Session::flash('alert-success', 'Order Created Successfully.');
            return Redirect::to('customer-order')->withInput()->withErrors($validator);
        }

    }


    public function getCustomerOrder()
    {
	    $user_id = Input::get('user_id');
        return json_encode(CustomerOrder::where('created_by_id',$user_id)->get());
    }

	public function getCustomerOrderAdmin()
    {
        return json_encode(CustomerOrder::all());
    }

	public function acceptCustomerOrder($ordernum)
	{

			 CustomerOrder::where('order_number', '=', $ordernum)->update(['status' => 1]);

			 $data = CustomerOrder::where('order_number', '=', $ordernum)->get();
			 $user = User::find($data[0]->created_by_id);

			 $sign_link = md5($ordernum . $user->email);

			 CustomerOrder::where('order_number', '=', $ordernum)->update(['sign_link' => $sign_link]);

			Mail::send('emails.message', ['user' => $user->name, 'order_num' => $ordernum, 'sign_link' => $sign_link, 'data' => $data[0] ], function ($m) use ($user, $data) {
				$m->to($user->email, '')->subject('Custtomer Order Accepted');
			});

			 Session::flash('alert-success', 'Order Accepted.');
			 return Redirect::to('home');
	}

	public function rejectCustomerOrder($ordernum)
	{
		CustomerOrder::where('order_number', '=', $ordernum)->update(['status' => 2]);
		$data = CustomerOrder::where('order_number', '=', $ordernum)->get();
		$user = User::find($data[0]->created_by_id);

		Mail::send('emails.message-rejected', ['user' => $user->name, 'order_num' => $ordernum, 'reject_reason' => Input::get('reject_reason'), 'data' => $data[0]], function ($m) use ($user, $data) {
			$m->to($user->email, '')->subject('Custtomer Order Rejected');
		});


		Session::flash('alert-success', 'Order Rejected.');
		return Redirect::to('home');
	}

	public function showCustomerOrderAdmin($id)
	{
		$corder_view = CustomerOrder::find($id);
        return View::make('order.corder_view')->with('corder_view', $corder_view);
	}

	public function showAcceptedSign($md5)
	{
		$data = CustomerOrder::where('sign_link', '=', $md5)->get();
		return view('order.accepted-sign')->with(array('data' => $data));
	}

	public function postAcceptedSign($md5)
	{
		$sign_draw  = Input::get('output');
		CustomerOrder::where('sign_link', '=', $md5)->update(['sign' => $sign_draw]);


	  $notif = CustomerOrder::where('sign_link', '=', $md5)->get();

		Mail::send('emails.signed-notif', ['order_num' => $notif[0]->order_number], function ($m) use ($notif) {
			$m->to('mis@qdf-phils.com', '')->subject('Customer Order '.$notif[0]->order_number.' is now signed');
		});

		Session::flash('alert-success', 'Order Signed.');

	  return Redirect::to('home');
	}

    public function showCustomerOrderList()
    {
        return view('order.customer-order-list');
    }

    public function getOrderList()
    {

    $getOrderList = CustomerOrder::select(['*']);
    return Datatables::of($getOrderList)
        ->addColumn('order_number_link', function ($getOrderList) {
            return '<a href=customer-order/'.$getOrderList->id.' class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i> '.$getOrderList->order_number.'</a>';
        })
        ->addColumn('rate', function ($getOrderList) {
            return number_format($getOrderList->rate,2);
        })
        ->make(true);
    }

}
