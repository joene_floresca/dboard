@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Supplier List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					
                    <table id="supplierList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            	<th colspan="6"> <center>Supplier Information<center></th>
                                <th colspan="2"> <center>Actions<center></th>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Company Name</th>
                                <th>Company Alias</th>
                                <th>Company Owner</th>
                                <th>Website</th>
                                <th>Type</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table> 

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('supplier-index')
<script>
$('#supplierList').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'api/supplier/all',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'CompanyName', name: 'CompanyName'},
            {data: 'CompanyAlias', name: 'CompanyAlias'},
            {data: 'CompanyOwner', name: 'CompanyOwner'},
            {data: 'Website', name: 'Website'},
            {data: 'Type', name: 'Type'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
});
</script>		
@endsection