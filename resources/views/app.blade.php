<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CTS</title>


	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/override.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/jquery.signaturepad.css') }}" rel="stylesheet">
	<!-- <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ asset('bootflat/css/bootflat.css') }}" rel="stylesheet">
	<link href="{{ asset('bootflat/css/bootflat.css.map') }}" rel="stylesheet"> -->


	<!-- Fonts -->
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

	<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->
	<link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">




	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
    body { font: normal 100.01%/1.375 "Helvetica Neue",Helvetica,Arial,sans-serif; }
  </style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">CTS</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">

					@if (Auth::check())
						@if(Auth::user()->user_level == 1)
							<li><a href="{{ url('home') }}">Dashboard</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Clients"><span class="glyphicon glyphicon-globe"></span><!-- Clients --><span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('client') }}">Client List</a></li>
									<li><a href="{{ url('client/create') }}">Add Client</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Suppliers"><span class="glyphicon glyphicon-inbox"></span><!-- Suppliers --><span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('supplier') }}">Supplier List</a></li>
									<li><a href="{{ url('supplier/create') }}">Add Supplier</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Campaigns"><span class="glyphicon glyphicon-bullhorn"></span><!-- Campaigns --><span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('campaign') }}">Campaign List</a></li>
									<li><a href="{{ url('campaign/create') }}">Add Campaign</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Questions"><span class="glyphicon glyphicon-question-sign"></span><!--  Questions --> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('question') }}">Question List</a></li>
									<li><a href="{{ url('question/create') }}">Add Question</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Order Confirmation"><span class="glyphicon glyphicon-list-alt"></span><!-- Order Confirmation --><span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('order/create') }}">Add Order</a></li>
									<li><a href="{{ url('order') }}">Order List</a></li>
									<li><a href="{{ url('customer-order') }}">Add Customer Order</a></li>
									<li><a href="{{ url('customer-order-list') }}">Customer Order List</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Purchase Order"><span class="glyphicon glyphicon-shopping-cart"></span><!-- Purchase Order --><span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('po') }}">Purchase Order List</a></li>
									<li><a href="{{ url('po/create') }}">Add Purchase Order</a></li>
								</ul>
							</li>
						@endif
						@if(Auth::user()->user_level == 0)
							<li><a href="{{ url('home') }}">Dashboard</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" data-toggle="tooltip" title="Order Confirmation"><span class="glyphicon glyphicon-list-alt"></span><!-- Order Confirmation --><span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ url('customer-order') }}">Add Customer Order</a></li>
								</ul>
							</li>
						@endif
					@endif

					
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp; &nbsp;{{ Auth::user()->name }}<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')



	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="{{ asset('bootflat/js/icheck.min.js') }}"></script>
	<script src="{{ asset('bootflat/js/jquery.fs.selecter.min.js') }}"></script>
	<script src="{{ asset('bootflat/js/jquery.fs.stepper.min.js') }}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script src="{{ asset('js/jquery.progressTimer.js') }}"></script>
	<script src="{{ asset('js/script.js') }}"></script>
	<script src="{{ asset('js/summernote.js') }}"></script>
	<!-- <script src="{{ asset('js/flashcanvas.js') }}"></script> -->
	<script src="{{ asset('js/jquery.signaturepad.js') }}"></script>

	<script>

	  $(function() {
	    $( "#fromdate, #todate, #orderDate, #orderPoDate, #orderFirstDeliveryDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
	  });

	$("#CampaignName").change(function() {
		var CampaignName = $("#CampaignName option:selected").text()
  		$("#QuestionCode").val(CampaignName.replace(/ /g,'_'));
  		// .replace(/ /g,'')
	});

	$(document).on("click", "#btnGenerate", function() {
  		var num = parseInt($("#numGenerate").val());
	     var html = '';
	     for(var i = 0; i < num ; i++)
	     {
	     	html = '<tr><td>Script</td><td><textarea name="script[]"> Content here.. </textarea></td><td><input type="text" class="form-control" placeholder="Enter Response" name="response[]" /><td></tr>';
	     	$('#scripts').append(html);
	     }
	     $('textarea').summernote();
	     $('#NumberOfScripts').val(i);
	});


  </script>
  @yield('customer-create')
  @yield('customer-index')
  @yield('campaign-index')
  @yield('supplier-index')
  @yield('location-index')
  @yield('question-index')
  @yield('client-index')
  @yield('customer-dasboard')
  @yield('admin-order-dasboard')
  @yield('order-create')
  @yield('customerOrderIndex-index')
  @yield('sign-accepted')

</body>
</html>
