@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Home</div>

				<div class="panel-body">

					@if (Auth::check())
						@if(Auth::user()->user_level == 0)
						<!-- Customer Dashboard -->
							<div class="container-fluid">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-primary">
											<div class="panel-heading">Customer - Order List</div>
											<div class="panel-body">
												@if (count($errors) > 0)
													<div class="alert alert-danger">
														<strong>Whoops!</strong> There were some problems with your input.<br><br>
														<ul>
															@foreach ($errors->all() as $error)
																<li>{{ $error }}</li>
															@endforeach
														</ul>
													</div>
												@endif
												<div class="flash-message">
											        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
											          @if(Session::has('alert-' . $msg))
											          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
											          @endif
											        @endforeach
										        </div>
												<div class="loading-progress" id="progressbar" style="padding-left: 2px; padding-right: 2px; padding-top: 2px"></div>
												<div class="col-md-3">
													<div class="sidebar">
															<div class="panel panel-warning">
																<div class="panel-heading">Status</div>
																	<div class="panel-body">
																		<ul class="nav navbar-nav col-md-12">
																			<li class="active" id="approved_filter"><a href="#"><span class="glyphicon glyphicon-ok"></span> Approved</a></li>
																			<li id="rejected_filter"><a href="#"><span class="glyphicon glyphicon-remove"></span> Rejected</a></li>
																			<li id="pending_filter"><a href="#"><span class="glyphicon glyphicon-flag"></span> Pending</a></li>
																		</ul>
																	</div>
															</div>
													</div>
												</div>
												<div class="col-md-9">
				                    <table id="orderList" class="table table-striped table-bordered" cellspacing="0" width="100%">
				                        <thead>
				                            <tr>
				                                <th>Order Number</th>
				                                <!-- <th>Question</th> -->
				                                <th>Rate</th>
				                                <th>PDF Download</th>
				                                <th>Suppression Download</th>
				                                <th>Status</th>
				                                <th>Action</th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                        </tbody>
				                    </table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif

							@if(Auth::user()->user_level == 1)
							<!-- Admin Dashboard -->

								<div class="container-fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="panel panel-primary">
												<div class="panel-heading">Administrator - Order List</div>
												<div class="panel-body">
													@if (count($errors) > 0)
														<div class="alert alert-danger">
															<strong>Whoops!</strong> There were some problems with your input.<br><br>
															<ul>
																@foreach ($errors->all() as $error)
																	<li>{{ $error }}</li>
																@endforeach
															</ul>
														</div>
													@endif
													<div class="flash-message">
																@foreach (['danger', 'warning', 'success', 'info'] as $msg)
																	@if(Session::has('alert-' . $msg))
																	<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
																	@endif
																@endforeach
															</div>
													<div class="loading-progress" id="progressbar" style="padding-left: 2px; padding-right: 2px; padding-top: 2px"></div>
													<div class="col-md-2">
														<div class="sidebar">
																<div class="panel panel-warning">
																	<div class="panel-heading">Status</div>
																		<div class="panel-body">
																			  <ul class="nav navbar-nav col-sm-12">
																	            <li class="active" id="approved_filter"><a href="#"><span class="glyphicon glyphicon-ok"></span>&nbsp;Approved</a></li>
																	            <li id="rejected_filter"><a href="#"><span class="glyphicon glyphicon-remove"></span>&nbsp;Rejected</a></li>
																	            <li id="pending_filter"><a href="#"><span class="glyphicon glyphicon-flag"></span>&nbsp;Pending</a></li>
																	          </ul>
																		</div>
																</div>
														</div>
													</div>
													<div class="col-md-9">
															<table id="orderList" class="table table-striped table-bordered" cellspacing="0" width="100%">
																	<thead>
																			<tr>
																					<th>Order Number</th>
																					<!-- <th>Question</th> -->
																					<th>Rate</th>
																					<th>PDF Download</th>
																					<th>Suppression Download</th>
																					<th>Status</th>
																					<th>Accept</th>
																					<th>Reject</th>
																					<th>Action</th>
																			</tr>
																	</thead>
																	<tbody>
																	</tbody>
															</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endif


					@endif
					<div class="modal fade" id="confirm-accept" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				      <div class="modal-content">

				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				          <h4 class="modal-title" id="myModalLabel">Confirm Accept</h4>
				        </div>

				        <div class="modal-body">
				          <p>You are about to accept order, this procedure is irreversible.</p>
				          <p>Do you want to proceed?</p>
				          <p class="debug-url"></p>
				        </div>

				        <div class="modal-footer">
				          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				          <a class="btn btn-success btn-ok">Accept</a>
				        </div>
				      </div>
				    </div>
				  </div>

					<div class="modal fade" id="confirm-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				      <div class="modal-content">

				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				          <h4 class="modal-title" id="myModalLabel">Confirm Reject</h4>
				        </div>

				        <div class="modal-body">
				          <p>You are about to reject order, this procedure is irreversible.</p>
				          <p>Do you want to proceed?</p>
				          <p class="debug-url"></p>

									<input type="text" class="form-control" name="reject_reason" id="reject_reason" placeholder="Enter reject reason">
				        </div>

				        <div class="modal-footer">
				          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				          <a class="btn btn-danger btn-ok" id="btnReject">Reject</a>
				        </div>
				      </div>
				    </div>
				  </div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@if (Auth::check())
	@if(Auth::user()->user_level == 0)
		@section('customer-dasboard')
		<script>
		$(document).ready(function()
		{
			$("li#approved_filter").on("click", function(){
					var oTable = $('#orderList').dataTable();
					oTable.fnFilter('Approved');
			});

			$("li#rejected_filter").on("click", function(){
					var oTable = $('#orderList').dataTable();
					oTable.fnFilter('Rejected');
			});

			$("li#pending_filter").on("click", function(){
					var oTable = $('#orderList').dataTable();
					oTable.fnFilter('Pending');
			});
		});
		var user_id = '{{Auth::user()->id}}' ;
		$.ajax({
				url: "get-customer-order",
				type: 'GET',
				data: {"user_id" : parseInt(user_id)},
				success: function(result){
				var myObj = $.parseJSON(result);
							$.each(myObj, function(key,value) {
								var t = $('#orderList').DataTable();

									var status;

								if (value.status == 0) {
									status = "Pending";
							} else if (value.status == 1) {
									status = "Approved";
							} else if (value.status == 2) {
									status = "Rejected";
							} else {
								status = "N/A";
							}
								t.row.add( [
											//value.order_number,
											'<button class="btn btn-warning">'+'<a href="customer-order/'+value.id+'">'+value.order_number+'</a>'+'</span></button>',
											//value.question,
											value.rate,
											"<a href="+value.other_info_link+" target='_blank'>Download </a>",
											"<a href="+value.suppression_info_link+" target='_blank'>Download </a>",
											status,
											"<a href=sign-accepted/"+value.sign_link+" >Sign</a>"
									] ).draw();

						});
				}});
		</script>
		@endsection
	@endif

	@if(Auth::user()->user_level == 1)
		@section('admin-order-dasboard')
		<script>
		$(document).ready(function()
		{
			$("li#approved_filter").on("click", function(){
					var oTable = $('#orderList').dataTable();
				  oTable.fnFilter('Approved');
			});

			$("li#rejected_filter").on("click", function(){
					var oTable = $('#orderList').dataTable();
				  oTable.fnFilter('Rejected');
			});

			$("li#pending_filter").on("click", function(){
					var oTable = $('#orderList').dataTable();
				  oTable.fnFilter('Pending');
			});
		});




		$.ajax({
				url: "get-customer-order-admin",
				type: 'GET',
				success: function(result){
				var myObj = $.parseJSON(result);
							$.each(myObj, function(key,value) {
								var t = $('#orderList').DataTable();

									var status;

								if (value.status == 0) {
									status = "Pending";
							} else if (value.status == 1) {
									status = "Approved";
							} else if (value.status == 2) {
									status = "Rejected";
							} else {
								status = "Deleted";
							}

								t.row.add( [
											'<button class="btn btn-warning">'+'<a href="customer-order/'+value.id+'">'+value.order_number+'</a>'+'</span></button>',
											//value.question,
											value.rate.toFixed(2),
											"<a href="+value.other_info_link+" target='_blank'>Download </a>",
											"<a href="+value.suppression_info_link+" target='_blank'>Download </a>",
											status,

											'<button class="btn btn-success" data-href="accept-order/'+value.order_number+'" data-toggle="modal" data-target="#confirm-accept"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>',
											'<button class="btn btn-warning" data-href="reject-order/'+value.order_number+'?reject_reason=" data-toggle="modal" data-target="#confirm-reject"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>',
											"<a href=sign-accepted/"+value.sign_link+" >Sign</a>"
									] ).draw();

						});
				}});

				$('#confirm-accept').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

            $('.debug-url').html('Accept URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });

			$('#confirm-reject').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

            $('.debug-url').html('Reject URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });

				$("#reject_reason").keyup(function() {
						var append_reason =  $('#reject_reason').val();
						$("#btnReject").attr('href', 'reject-order/4365?reject_reason='+append_reason);
			  });


		</script>
		@endsection
	@endif

@endif
