@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Purchase Order</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
			         <form class="form-horizontal" role="form" method="POST" action="{{ url('applicant') }}">
			         	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				      <fieldset>
					      <div class="tabbable">
					        <ul class="nav nav-tabs" id="tab_bar">
					          <li class="active"><a href="#tab1" data-toggle="tab"><i class="glyphicon glyphicon-pencil"></i> Contact Information</a></li>
					          <li><a href="#tab2" data-toggle="tab"><i class="glyphicon glyphicon-user"></i> Supplier Details</a></li>
					          <li><a href="#tab3" data-toggle="tab"><i class="glyphicon glyphicon-list-alt"></i> Order Details</a></li>
					          <li><a href="#tab4" data-toggle="tab"><i class="glyphicon glyphicon-briefcase"></i> Delivery Instructions</a></li>
					          <li><a href="#tab5" data-toggle="tab"><i class="glyphicon glyphicon-paperclip"></i> Payment</a></li>
					        </ul>
					        
					        <div class="tab-content">
					          <!-- TAB 1 -->
					          <div class="tab-pane active" id="tab1">

					            <div class="control-group">
					              <label class="control-label" for="id_title">Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_name" id="contact_name" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Company</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_company" id="contact_company" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_address" id="contact_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Postcode</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_postcode" id="contact_postcode" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Telephone No</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_telephone" id="contact_telephone" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Email Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_email" id="contact_email" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Send Invoice to</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_invoice_email" id="contact_invoice_email" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Header</label>
					                <div class="controls">
					                  	<input class="form-control" name="contact_header" id="contact_header" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Quantity Required</label>
					                <div class="controls">
					                  	<input class="form-control" name="po_qty" id="po_qty" />
					                </div>
					            </div>

					             <div class="control-group">
					              <label class="control-label" for="id_title">Price Per Lead</label>
					                <div class="controls">
					                  	<input class="form-control" name="price_per_lead" id="price_per_lead" />
					                </div>
					            </div>


					            

					           </div>
					          
					          <!-- TAB 2 -->
					          <div class="tab-pane pre-scrollable" id="tab2">

					            <div class="control-group">
					              <label class="control-label" for="id_title">Supplier Name</label>
					                <div class="controls">
					                  	{!! Form::select('supplier_name', $supplier_options, '',array('class' => 'form-control', 'id' => 'supplier_name')) !!}
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Contact Person</label>
					                <div class="controls">
					                  	<input class="form-control" name="supplier_contact" id="supplier_contact" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Supplier Contact Number</label>
					                <div class="controls">
					                  	<input class="form-control" name="supplier_contact_num" id="supplier_contact_num" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Supplier Email</label>
					                <div class="controls">
					                  	<input class="form-control" name="supplier_email" id="supplier_email" />
					                </div>
					            </div>

					             <div class="control-group">
					              <label class="control-label" for="id_title">Supplier Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="supplier_address" id="supplier_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Date</label>
					                <div class="controls">
					                  	<input class="form-control" name="supplier_date" id="supplier_date" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">PO #</label>
					                <div class="controls">
					                  	<input class="form-control" name="supplier_po" id="supplier_po" />
					                </div>
					            </div>


					          </div>

					          <!-- TAB 3 -->
					          <div class="tab-pane pre-scrollable" id="tab3">

					          	<div class="control-group">
					              <label class="control-label" for="id_title">Reference </label>
					                <div class="controls">
					                	<input class="form-control" name="reference" id="reference" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">End User </label>
					                <div class="controls">
					                	<input class="form-control" name="end_user" id="end_user" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Order Type </label>
					                <div class="controls">
					                	<select name="orderType" id="orderType" class="form-control">
											<option value="">Choose One</option>
											<option value="SLG - Lifestyle - Generic">SLG - Lifestyle - Generic</option>
											<option value="SLG - Lifestyle">SLG - Lifestyle</option> <!-- Updated 26-Aug-2013 -->
											<option value="SLG - MCS">SLG - MCS</option> <!-- Updated 26-Aug-2013 -->
						                    <option value="SLG - MCS - Generic">SLG - MCS - Generic</option> <!-- Updated 06-June-2014 -->
											<option value="Historic/Back Data">Historic/Back Data</option> <!-- Updated 26-Aug-2013 -->
											<option value="Mailing Data">Mailing Data</option> <!-- Updated 26-Aug-2013 -->
											<option value="Call Center Services">Call Center Services</option> <!-- Updated 26-Aug-2013 -->
											<option value="Tele-Appending Services">Tele-Appending Services</option> <!-- Updated 26-Aug-2013 -->
											<option value="Data Management Services">Data Management Services</option> <!-- Updated 26-Aug-2013 -->
											<option value="Data Supply Services">Data Supply Services</option> <!-- Updated 26-Aug-2013 -->
											<option value="SLG - MCS - Gerenic">SLG - MCS - Gerenic</option>
						                    <option value="Customer Service">Customer Service</option>				
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Order Type </label>
					                <div class="controls">
					                	<select name="orderCategory" id="orderCategory" class="form-control">
											<option value="">Choose One</option>
											<option value="Children Charity">Children Charity</option>
											<option value="Health and Research Charity">Health and Research Charity</option>
											<option value="Animal Charity">Animal Charity</option>
											<option value="International and Humanitarian Charitiy">International and Humanitarian Charitiy</option>
											<option value="Elderly Charity">Elderly Charity</option>
											<option value="Youth Charity">Youth Charity</option>
											<option value="Environment Charity">Environment Charity</option>
											<option value="Utilities">Utilities</option>
											<option value="TV Sky">TV Sky</option>
											<option value="Washing Machine">Washing Machine</option>
											<option value="Excessive Noise">Excessive Noise</option>
											<option value="Road Traffic Accident">Road Traffic Accident</option>
											<option value="Accident at Work">Accident at Work</option>
											<option value="Medical Negligence">Medical Negligence</option>
											<option value="Industrial Diseases">Industrial Diseases</option>
											<option value="Direct Debit">Direct Debit</option>
											<option value="Conservatory">Conservatory</option>
											<option value="Newspaper">Newspaper</option>
											<option value="Magazines">Magazines</option>
											<option value="Endowment Mortgage">Endowment Mortgage</option>
											<option value="Fuel Voucher">Fuel Voucher</option>
											<option value="Debt Management">Debt Management</option>
											<option value="Life Insurance">Life Insurance</option>
											<option value="Search and Rescue">Search and Rescue</option>
											<option value="Medical Insurance">Medical Insurance</option>
											<option value="Education Charity">Education Charity</option>
											<option value="Arts and Culture Charity">Arts and Culture Charity</option>
											<option value="Family Charity">Family Charity</option>
											<option value="Funeral">Funeral</option>				
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Age </label>
					                <div class="controls">
					                	<input class="form-control" name="age" id="age" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Must be Homeowners? </label>
					                <div class="controls">
					                	<select name="must_homeowner" id="must_homeowner" class="form-control">
											<option value="">Choose One</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
											<option value="N/A">N/A</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Telephone </label>
					                <div class="controls">
					                	<select name="telephone" id="telephone" class="form-control">
											<option value="">Choose One</option>
											<option value="Landline">Landline</option>
											<option value="Mobile">Mobile</option>
											<option value="Landline/Mobile">Landline/Mobile</option>
											<option value="N/A">N/A</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Postcode Exclusions (If any) </label>
					                <div class="controls">
					                	<input type="text" class="form-control" name="PostcodeExclusion" id="PostcodeExclusion" value="{{ old('PostcodeExclusion') }}"> 
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Covered Areas </label>
					                <div class="controls">
					                	<input type="text" class="form-control" name="CoveredAreas" id="CoveredAreas" value="{{ old('CoveredAreas') }}" placeholder="Indicate if England, Scotland, Wales, NI">
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Data Hygiene </label>
					                <div class="controls">
					                	<select name="orderDataHygiene" id="orderDataHygiene" class="form-control">
											<option value="">Choose One</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
											<option value="N/A">N/A</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Other Conditions (If any) </label>
					                <div class="controls">
					                	<input type="text" class="form-control" name="other_conditions" id="other_conditions" value="{{ old('other_conditions') }}"> 
					                </div>
					            </div>

					          </div>

					          <!-- TAB 4 -->
					          <div class="tab-pane pre-scrollable" id="tab4">

					          	<!-- Company 1 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">First Delivery</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1" id="company1" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">First Upload Recordings(5days after first delivery)</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_address" id="company1_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Delivery Method</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_department" id="company1_department" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Delivery Schedule</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_position" id="company1_position" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Email Delivery Confirmation to the ff:</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_salary" id="company1_salary" />
					                </div>
					            </div>

					            <!-- Company 2 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">Delivery Format or Layout</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2" id="company2" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Other Delivery Instructions</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_address" id="company2_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Status</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_department" id="company2_department" />
					                </div>
					            </div>

					           

					          </div>

					          <!-- TAB 5 -->
					          <div class="tab-pane" id="tab5">





								<div class="col-sm-4">

								



					          		<div class="control-group">
						              <label class="control-label" for="id_title">Sub Total(as per above)</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder=" " />
						                </div>
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">Development Fee</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder=" " />
						                </div>
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">Commisions</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder=" " />
						                </div>
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">Total Order Value</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder=" " />
						                </div>
						            </div>

								</div>


								<div class="col-sm-4">

									<div class="control-group">
										<div class="spaceB">	
						                </div>

										<div class="controls">
											<label class="control-label" for="id_title"><b>PAYMENT PROOF</b></label>
										</div>	

										<div class="spaceC">	
						                </div>		                
									</div>

									<div class="control-group">
						              <label class="control-label" for="id_title">&nbsp;</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder="Delivery Notification" />
						                </div>
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">&nbsp;</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder="Data Hygiene Result" />
						                </div>
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">&nbsp;</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder="PO Number" />
						                </div>
						            </div>

								</div>


								    <div class="col-sm-4">
									<div class="control-group">
										<div class="spaceB">	
						                </div>
										<div class="controls">
											<label class="control-label" for="id_title"><b>PAYMENT TERMS</b></label>
										</div>	
										<div class="spaceC">	
						                </div>				                
									</div>

									<div class="control-group">
						              <label class="control-label" for="id_title">&nbsp;</label>
						                <div class="spaceA">
						                  	
						                </div>	
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">&nbsp;</label>
						                <div class="controls">
						                  	<input class="form-control" name=" " id=" " placeholder="30 days" />
						                </div>
						            </div>

						            <div class="control-group">
						              <label class="control-label" for="id_title">&nbsp;</label>
						                <div class="spaceA">
						                  	
						                </div>	
						            </div>

								</div>


							


						            

						         


						           
					          </div>



						<span class="clearfix"></span>

					        </div>
					    <hr class="border-line"> <!-- STYLED IN FORMS.CSS -->
					    <div class="form-group">
							<div class="col-md-6">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
				      </fieldset>
				    </form>
					<div class="btn-group">
					    <button class="btn btn-warning" id="prevtab" type="button">Prev</button>
					    <button class="btn btn-warning" id="nexttab" type="button">Next</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection

