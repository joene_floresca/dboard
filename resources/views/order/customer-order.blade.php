@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading"><span class="glyphicon glyphicon-plus"></span>Add Customer Order</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					<form class="form-horizontal" role="form" method="POST" action="{{ url('customer-order') }}" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Question</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="question" id="question" value="{{ old('question') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Rate</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="rate" id="rate">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Currency</label>
							<div class="col-md-6">
								<select class="form-control" name="orderCurrency" id="orderCurrency">
									<option>Choose One</option>
									<option value="AUD">AUD</option>
									<option value="GBP">GBP</option>
									<option value="USD">USD</option>
							    </select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">First Delivery Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderFirstDeliveryDate" id="orderFirstDeliveryDate">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Schedule</label>
							<div class="col-md-6">
								<!-- <input type="text" class="form-control" name="orderDeliverySched" id="orderDeliverySched"> -->
								<select name="orderDeliverySched" id="orderDeliverySched" class="form-control">
									<option value="">Choose One</option>
									<option value="Daily">Daily</option>
									<option value="Custom">Custom</option>
								</select>
							</div>
						</div>

						<div class="form-group" style="display: none" id="divCustomDelivery">
							<label class="col-md-4 control-label">Custom Delivery Schedule</label>
							<div class="col-md-6">
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayMon" id="checkDayMon" value="Monday">Monday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayTues" id="checkDayTues" value="Tuesday">Tuesday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayWed" id="checkDayWed" value="Wednesday">Wednesday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayTh" id="checkDayTh" value="Thursday">Thursday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayFr" id="checkDayFr" value="Friday">Friday</label>
								</div>
								<div class="text" style="padding-top: 4px">
									<input type="text" class="form-control" name="orderMainDeliverySched" id="orderMainDeliverySched">
								</div>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label">Payment Terms</label>
							<div class="col-md-6">
								<select name="orderPaymentTerms" id="orderPaymentTerms" class="form-control">
									<option value="">Choose One</option>
									<option value="Upfront">Upfront</option>
									<option value="5 days">5 days</option>
									<option value="7 days">7 days</option>
									<option value="14 days">14 days</option>
									<option value="30 days">30 days</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Upload other Info</label>
							<div class="col-md-6">
								<input type="file" class="filestyle" name="uploadOtherInfo" data-buttonName="btn-primary">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Upload Suppression Files / Numbers</label>
							<div class="col-md-6">
  								<input type="file" class="filestyle" name="UploadSuppressionFiles" data-buttonName="btn-primary">
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label">Remarks</label>
							<div class="col-md-6">
								<textarea name="orderRemarks" id="orderRemarks" class="form-control"></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('customer-create')
<script type="text/javascript">
$("#orderDeliverySched").change(function() {
  	if($("#orderDeliverySched").val() == "Daily")
  	{
  		$("#orderMainDeliverySched").val("");
  		$("#orderMainDeliverySched").val("Daily");
  		$("#divCustomDelivery").css("display", "none");
  	}
  	else
  	{
  		$("#orderMainDeliverySched").val("");
  		$("#divCustomDelivery").css("display", "block");
  	}

});

$("#checkDayMon").click(function() {
	var value = $("#checkDayMon").val();
	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayTues").click(function() {
	var value = $("#checkDayTues").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayWed").click(function() {
	var value = $("#checkDayWed").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayTh").click(function() {
	var value = $("#checkDayTh").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayFr").click(function() {
	var value = $("#checkDayFr").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$(":file").filestyle({buttonName: "btn-primary"});
</script>
@endsection
