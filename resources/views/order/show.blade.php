@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">Order Information</div>
				<div class="panel-body">
					

					<div class="form-horizontal">
						
						<div class="form-group">
							<label class="col-md-4 control-label">ID</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->id }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Date</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->date }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->currency }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Currency</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->country }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Client</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->client }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Ref. Number</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->ref_number }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">OC #</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->order_confirmnation }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO #</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->po_number }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Date</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->po_date }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">End User</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->end_user }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Category</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->category }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Type</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->type }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Amount</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->amount }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Count</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->po_count }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">OC Count</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->oc_count }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">First Delivery Date</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->first_delivery }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Schedule</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->delivery_sched }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Method</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->delivery_method }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">File Format</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->file_format }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data Hygiene</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->data_hygiene }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Other Instruction</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->other_instruction }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Billing Schedule</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->billing_schedule }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Payment Terms</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->payment_terms }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Sub Total</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->sub_total }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Development Fee</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->development_fee }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Comission</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->comission }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Total Amount</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->order_total }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivered By</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->delivered_by }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Remarks</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->remarks }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Total Delivered</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $order->total_delivered }}</label>
							</div>
						</div>

						

					</div>	
						
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
