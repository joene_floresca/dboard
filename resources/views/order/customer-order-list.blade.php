@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Order List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					<div class="loading-progress" id="progressbar" style="padding-left: 2px; padding-right: 2px; padding-top: 2px"></div>
                    <table id="orderList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            	<th colspan="7"> <center>Order Information<center></th>
                                
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Order no.</th>
                                <th>First Delivery date</th>
                                <th>Delivery Schedule</th>
                                <th>Rate</th>
                                <th>Currency</th>
                                <th>Date Created</th>
                              
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customerOrderIndex-index')

<script>
	$('#orderList').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'order-list',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'order_number_link', name: 'order_number_link'},
            {data: 'first_delivery_date', name: 'first_delivery_date'},
            {data: 'delivery_schedule', name: 'delivery_schedule'},
            {data: 'rate', name: 'rate'},
            {data: 'currency', name: 'currency'},
            {data: 'created_at', name: 'created_at'}
        ]
    });
</script>
@endsection
