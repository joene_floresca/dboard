@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">Edit Order Information</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					{!! Form::model($order, array('route' => array('order.update', $order->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<div class="form-group">
							<label class="col-md-4 control-label">Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderDate" id="orderDate" value="{{ $order->date }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Client</label>
							<div class="col-md-6">
								{!! Form::select('orderClient', $client_options, $order->client ,array('class' => 'form-control', 'id' => 'orderClient')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-6">
								{!! Form::select('orderCountry', ['' => 'Choose One', 'UK' => 'UK', 'NZ' => 'NZ', 'AU' => 'AU'], $order->country, array('class' => 'form-control', 'id' => 'orderCountry')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Currency</label>
							<div class="col-md-6">
								{!! Form::select('orderCurrency', ['' => 'Choose One', 'AUD' => 'AUD', 'GBP' => 'GBP', 'USD' => 'USD'], $order->currency, array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Ref. Number</label>
							<div class="col-md-6">
								<span id="oc_name" name="oc_name"></span>
								<input type="text" class="form-control" name="orderOcNumber" id="orderOcNumber" readonly value="{{$order->ref_number}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Order Confirmation</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderConfirmation" id="orderConfirmation" value="{{$order->order_confirmnation}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="poNumber" id="poNumber" value="{{$order->po_number}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderPoDate" id="orderPoDate" value="{{$order->po_date}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">End User</label>
							<div class="col-md-6">
								{!! Form::select('orderEndUser', $campaign_options, $order->end_user ,array('class' => 'form-control', 'id' => 'orderEndUser')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Category</label>
							<div class="col-md-6">
								<select name="orderCategory" id="orderCategory" class="form-control">
									<option value="{{$order->category}}" selected>{{$order->category}}</option>
									<option value="">Choose One</option>
									<option value="Children Charity">Children Charity</option>
									<option value="Health and Research Charity">Health and Research Charity</option>
									<option value="Animal Charity">Animal Charity</option>
									<option value="International and Humanitarian Charitiy">International and Humanitarian Charitiy</option>
									<option value="Elderly Charity">Elderly Charity</option>
									<option value="Youth Charity">Youth Charity</option>
									<option value="Environment Charity">Environment Charity</option>
									<option value="Utilities">Utilities</option>
									<option value="TV Sky">TV Sky</option>
									<option value="Washing Machine">Washing Machine</option>
									<option value="Excessive Noise">Excessive Noise</option>
									<option value="Road Traffic Accident">Road Traffic Accident</option>
									<option value="Accident at Work">Accident at Work</option>
									<option value="Medical Negligence">Medical Negligence</option>
									<option value="Industrial Diseases">Industrial Diseases</option>
									<option value="Direct Debit">Direct Debit</option>
									<option value="Conservatory">Conservatory</option>
									<option value="Newspaper">Newspaper</option>
									<option value="Magazines">Magazines</option>
									<option value="Endowment Mortgage">Endowment Mortgage</option>
									<option value="Fuel Voucher">Fuel Voucher</option>
									<option value="Debt Management">Debt Management</option>
									<option value="Life Insurance">Life Insurance</option>
									<option value="Search and Rescue">Search and Rescue</option>
									<option value="Medical Insurance">Medical Insurance</option>
									<option value="Education Charity">Education Charity</option>
									<option value="Arts and Culture Charity">Arts and Culture Charity</option>
									<option value="Family Charity">Family Charity</option>
									<option value="Funeral">Funeral</option>				
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Type</label>
							<div class="col-md-6">
								<select name="orderType" id="orderType" class="form-control">
									<option value="{{$order->type}}" selected>{{$order->type}}</option>
									<option value="">Choose One</option>
									<option value="SLG - Lifestyle - Generic">SLG - Lifestyle - Generic</option>
									<option value="SLG - Lifestyle">SLG - Lifestyle</option> <!-- Updated 26-Aug-2013 -->
									<option value="SLG - MCS">SLG - MCS</option> <!-- Updated 26-Aug-2013 -->
				                    <option value="SLG - MCS - Generic">SLG - MCS - Generic</option> <!-- Updated 06-June-2014 -->
									<option value="Historic/Back Data">Historic/Back Data</option> <!-- Updated 26-Aug-2013 -->
									<option value="Mailing Data">Mailing Data</option> <!-- Updated 26-Aug-2013 -->
									<option value="Call Center Services">Call Center Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="Tele-Appending Services">Tele-Appending Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="Data Management Services">Data Management Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="Data Supply Services">Data Supply Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="SLG - MCS - Gerenic">SLG - MCS - Gerenic</option>
				                    <option value="Customer Service">Customer Service</option>				
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Amount</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderAmount" id="orderAmount" value="{{$order->amount}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Count</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderPoCount" id="orderPoCount" value="{{$order->po_count}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">OC Count</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderOcCount" id="orderOcCount" value="{{$order->oc_count}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">First Delivery Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderFirstDeliveryDate" id="orderFirstDeliveryDate" value="{{$order->first_delivery}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Schedule</label>
							<div class="col-md-6">
								<!-- <input type="text" class="form-control" name="orderDeliverySched" id="orderDeliverySched"> -->
								<select name="orderDeliverySched" id="orderDeliverySched" class="form-control">
									<option value="{{$order->delivery_sched}}" selected>{{$order->delivery_sched}}</option>
									<option value="">Choose One</option>
									<option value="Daily">Daily</option>
									<option value="Custom">Custom</option>
								</select>
							</div>
						</div>

						<div class="form-group" style="display: none" id="divCustomDelivery">
							<label class="col-md-4 control-label">Custom Delivery Schedule</label>
							<div class="col-md-6">
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayMon" id="checkDayMon" value="Monday">Monday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayTues" id="checkDayTues" value="Tuesday">Tuesday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayWed" id="checkDayWed" value="Wednesday">Wednesday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayTh" id="checkDayTh" value="Thursday">Thursday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayFr" id="checkDayFr" value="Friday">Friday</label>
								</div>
								<div class="text" style="padding-top: 4px"><input type="text" class="form-control" name="orderMainDeliverySched" id="orderMainDeliverySched" value="{{$order->delivery_sched}}"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Method</label>
							<div class="col-md-6">
								{!! Form::select('orderDeliveryMethod', ['' => 'Choose One', 'Via SFTP' => 'Via SFTP', 'Others' => 'Others'], $order->delivery_method, array('class' => 'form-control', 'id' => 'orderDeliveryMethod')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">File Format</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderFileFormat" id="orderFileFormat" value="{{$order->file_format}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Must be Ping, PAF, TPS Free</label>
							<div class="col-md-6">
								{!! Form::select('orderDataHygiene', ['' => 'Choose One', 'Yes' => 'Yes', 'No' => 'No', 'N/A' => 'N/A'], $order->data_hygiene, array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Other Delivery Instruction</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderDeliveryInstruction" id="orderDeliveryInstruction" value="{{$order->other_instruction}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Billing Schedule</label>
							<div class="col-md-6">
								{!! Form::select('orderBillingSched', ['' => 'Choose One', 'Daily' => 'Daily', 'Weekly' => 'Weekly', 'Bi-Monthly' => 'Bi-Monthly', 'Monthly' => 'Monthly'], $order->billing_schedule, array('class' => 'form-control')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Payment Terms</label>
							<div class="col-md-6">
								{!! Form::select('orderPaymentTerms', ['' => 'Choose One', 'Upfront' => 'Upfront', '5 days' => '5 days', '7 days' => '7 days', '14 days' => '14 days', '30 days' => '30 days'], $order->payment_terms, array('class' => 'form-control', 'id' => 'orderPaymentTerms')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Sub Total</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderSubTotal" id="orderSubTotal" value="{{$order->sub_total}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Development Fee</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderDevelopmentFee" id="orderDevelopmentFee" value="{{$order->development_fee}}" >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Commission</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderCommission" id="orderCommission" value="{{$order->comission}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Order Total Value</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderTotalValue" id="orderTotalValue" value="{{$order->order_total}}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">To be delivered by</label>
							<div class="col-md-6">
								{!! Form::select('orderDeliveredBy', ['' => 'Choose One', 'MIS' => 'MIS', 'Chris Quinn' => 'Chris Quinn', 'Others' => 'Others'], $order->delivered_by, array('class' => 'form-control', 'id' => 'orderDeliveredBy')) !!}
								
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Remarks</label>
							<div class="col-md-6">
								<textarea name="orderRemarks" id="orderRemarks" class="form-control">{{$order->remarks}}</textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Edit
								</button>
							</div>
						</div>

						
					{!! Form::close() !!}	

						
						
				</div>
			</div>

			

			


		</div>
	</div>
</div>
@endsection
