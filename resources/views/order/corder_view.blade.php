@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">Customer order Info</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

			        <div class="form-horizontal">
						<div class="form-group">
							  <label class="col-md-4 control-label">Question</label>
							<div class="col-md-6">
								<label class="labelValue" >{{ $corder_view->question }} </label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Rate</label>
							<div class="col-md-6">
								<label class="labelValue">{{ $corder_view->rate }} </label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Currency</label>
							<div class="col-md-6">
								<label class="labelValue">{{ $corder_view->currency }} </label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">First Delivery Date</label>
							<div class="col-md-6">
								<label class="labelValue">{{ $corder_view->first_delivery_date }} </label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Schedule</label>
							<div class="col-md-6">
								<label class="labelValue">{{ $corder_view->delivery_schedule }} </label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Payment Terms</label>
							<div class="col-md-6">
								<label class="labelValue">{{ $corder_view->payment_terms }} </label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Remarks</label>
							<div class="col-md-6">
								<label class="labelValue">{{ $corder_view->remarks }} </label>
							</div>
						</div>




					</div>	


					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
