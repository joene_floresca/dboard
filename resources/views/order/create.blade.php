@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading"><span class="glyphicon glyphicon-plus"></span>Add Order</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					<form class="form-horizontal" role="form" method="POST" action="{{ url('order') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderDate" id="orderDate" value="{{ old('orderDate') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Client</label>
							<div class="col-md-6">
								{!! Form::select('orderClient', $client_options, '',array('class' => 'form-control', 'id' => 'orderClient')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-6">
								<select class="form-control" name="orderCountry" id="orderCountry">
									<option>Choose One</option>
									<option value="UK">UK</option>
									<option value="NZ">NZ</option>
									<option value="AU">AU</option>
							    </select>

							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Currency</label>
							<div class="col-md-6">
								<select class="form-control" name="orderCurrency" id="orderCurrency">
									<option>Choose One</option>
									<option value="AUD">AUD</option>
									<option value="GBP">GBP</option>
									<option value="USD">USD</option>
							    </select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Ref. Number</label>
							<div class="col-md-6">
								<span id="oc_name" name="oc_name"></span>
								<input type="text" class="form-control" name="orderOcNumber" id="orderOcNumber" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Order Number</label>
							<div class="col-md-6">
								<span id="oc_name" name="oc_name"></span>
								<input type="text" class="form-control" name="orderNumber" id="orderNumber">
							</div>
						</div>

						<!-- <div class="form-group">
							<label class="col-md-4 control-label">Order Confirmation</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderConfirmation" id="orderConfirmation">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="poNumber" id="poNumber">
							</div>
						</div> -->

						<div class="form-group">
							<label class="col-md-4 control-label">PO Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderPoDate" id="orderPoDate">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">End User</label>
							<div class="col-md-6">
								{!! Form::select('orderEndUser', $campaign_options, '',array('class' => 'form-control', 'id' => 'orderEndUser')) !!}
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Category</label>
							<div class="col-md-6">
								<select name="orderCategory" id="orderCategory" class="form-control">
									<option value="">Choose One</option>
									<option value="Children Charity">Children Charity</option>
									<option value="Health and Research Charity">Health and Research Charity</option>
									<option value="Animal Charity">Animal Charity</option>
									<option value="International and Humanitarian Charitiy">International and Humanitarian Charitiy</option>
									<option value="Elderly Charity">Elderly Charity</option>
									<option value="Youth Charity">Youth Charity</option>
									<option value="Environment Charity">Environment Charity</option>
									<option value="Utilities">Utilities</option>
									<option value="TV Sky">TV Sky</option>
									<option value="Washing Machine">Washing Machine</option>
									<option value="Excessive Noise">Excessive Noise</option>
									<option value="Road Traffic Accident">Road Traffic Accident</option>
									<option value="Accident at Work">Accident at Work</option>
									<option value="Medical Negligence">Medical Negligence</option>
									<option value="Industrial Diseases">Industrial Diseases</option>
									<option value="Direct Debit">Direct Debit</option>
									<option value="Conservatory">Conservatory</option>
									<option value="Newspaper">Newspaper</option>
									<option value="Magazines">Magazines</option>
									<option value="Endowment Mortgage">Endowment Mortgage</option>
									<option value="Fuel Voucher">Fuel Voucher</option>
									<option value="Debt Management">Debt Management</option>
									<option value="Life Insurance">Life Insurance</option>
									<option value="Search and Rescue">Search and Rescue</option>
									<option value="Medical Insurance">Medical Insurance</option>
									<option value="Education Charity">Education Charity</option>
									<option value="Arts and Culture Charity">Arts and Culture Charity</option>
									<option value="Family Charity">Family Charity</option>
									<option value="Funeral">Funeral</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Type</label>
							<div class="col-md-6">
								<select name="orderType" id="orderType" class="form-control">
									<option value="">Choose One</option>
									<option value="SLG - Lifestyle - Generic">SLG - Lifestyle - Generic</option>
									<option value="SLG - Lifestyle">SLG - Lifestyle</option> <!-- Updated 26-Aug-2013 -->
									<option value="SLG - MCS">SLG - MCS</option> <!-- Updated 26-Aug-2013 -->
				                    <option value="SLG - MCS - Generic">SLG - MCS - Generic</option> <!-- Updated 06-June-2014 -->
									<option value="Historic/Back Data">Historic/Back Data</option> <!-- Updated 26-Aug-2013 -->
									<option value="Mailing Data">Mailing Data</option> <!-- Updated 26-Aug-2013 -->
									<option value="Call Center Services">Call Center Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="Tele-Appending Services">Tele-Appending Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="Data Management Services">Data Management Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="Data Supply Services">Data Supply Services</option> <!-- Updated 26-Aug-2013 -->
									<option value="SLG - MCS - Gerenic">SLG - MCS - Gerenic</option>
				                    <option value="Customer Service">Customer Service</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Amount</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderAmount" id="orderAmount">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Count</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderPoCount" id="orderPoCount">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">OC Count</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderOcCount" id="orderOcCount">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">First Delivery Date</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderFirstDeliveryDate" id="orderFirstDeliveryDate">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Schedule</label>
							<div class="col-md-6">
								<!-- <input type="text" class="form-control" name="orderDeliverySched" id="orderDeliverySched"> -->
								<select name="orderDeliverySched" id="orderDeliverySched" class="form-control">
									<option value="">Choose One</option>
									<option value="Daily">Daily</option>
									<option value="Custom">Custom</option>
								</select>
							</div>
						</div>

						<div class="form-group" style="display: none" id="divCustomDelivery">
							<label class="col-md-4 control-label">Custom Delivery Schedule</label>
							<div class="col-md-6">
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayMon" id="checkDayMon" value="Monday">Monday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayTues" id="checkDayTues" value="Tuesday">Tuesday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayWed" id="checkDayWed" value="Wednesday">Wednesday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayTh" id="checkDayTh" value="Thursday">Thursday</label>
								</div>
								<div class="checkbox">
 					 				<label><input type="checkbox" name="checkDayFr" id="checkDayFr" value="Friday">Friday</label>
								</div>
								<div class="text" style="padding-top: 4px"><input type="text" class="form-control" name="orderMainDeliverySched" id="orderMainDeliverySched"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Delivery Method</label>
							<div class="col-md-6">
								<select name="orderDeliveryMethod" id="orderDeliveryMethod" class="form-control">
									<option value="">Choose One</option>
									<option value="Via SFTP">Via SFTP</option>
									<option value="Others">Others</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">File Format</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderFileFormat" id="orderFileFormat">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Must be Ping, PAF, TPS Free</label>
							<div class="col-md-6">
								<select name="orderDataHygiene" id="orderDataHygiene" class="form-control">
									<option value="">Choose One</option>
									<option value="Yes">Yes</option>
									<option value="No">No</option>
									<option value="N/A">N/A</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Other Delivery Instruction</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderDeliveryInstruction" id="orderDeliveryInstruction">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Billing Schedule</label>
							<div class="col-md-6">
								<select name="orderBillingSched" id="orderBillingSched" class="form-control">
									<option value="">Choose One</option>
									<option value="Daily">Daily</option>
									<option value="Weekly">Weekly</option>
									<option value="Bi-Monthly">Bi-Monthly</option>
									<option value="Monthly">Monthly</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Payment Terms</label>
							<div class="col-md-6">
								<select name="orderPaymentTerms" id="orderPaymentTerms" class="form-control">
									<option value="">Choose One</option>
									<option value="Upfront">Upfront</option>
									<option value="5 days">5 days</option>
									<option value="7 days">7 days</option>
									<option value="14 days">14 days</option>
									<option value="30 days">30 days</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Sub Total</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderSubTotal" id="orderSubTotal">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Development Fee</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderDevelopmentFee" id="orderDevelopmentFee" value="Waived">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Commission</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderCommission" id="orderCommission">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Order Total Value</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="orderTotalValue" id="orderTotalValue">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">To be delivered by</label>
							<div class="col-md-6">
								<select name="orderDeliveredBy" id="orderDeliveredBy" class="form-control">
									<option value="">Choose One</option>
									<option value="MIS">MIS</option>
									<option value="Chris Quinn">Chris Quinn</option>
									<option value="Others">Others</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Remarks</label>
							<div class="col-md-6">
								<textarea name="orderRemarks" id="orderRemarks" class="form-control"></textarea>
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('order-create')
<script type="text/javascript">

$(document).ready(function() {
      $('.sigPad').signaturePad();
    });


$("#orderDeliverySched").change(function() {
  	if($("#orderDeliverySched").val() == "Daily")
  	{
  		$("#orderMainDeliverySched").val("");
  		$("#orderMainDeliverySched").val("Daily");
  		$("#divCustomDelivery").css("display", "none");
  	}
  	else
  	{
  		$("#orderMainDeliverySched").val("");
  		$("#divCustomDelivery").css("display", "block");
  	}
});

$("#checkDayMon").click(function() {
	var value = $("#checkDayMon").val();
	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayTues").click(function() {
	var value = $("#checkDayTues").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayWed").click(function() {
	var value = $("#checkDayWed").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayTh").click(function() {
	var value = $("#checkDayTh").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

$("#checkDayFr").click(function() {
	var value = $("#checkDayFr").val();
  	$('#orderMainDeliverySched').val(function(i,val) {
     	return val + (!val ? '' : ',') + value;
	});
});

</script>
<script src="{{ asset('js/json2.min.js') }}"></script>
@endsection
