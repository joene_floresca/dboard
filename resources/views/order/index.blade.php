@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Order List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					<div class="loading-progress" id="progressbar" style="padding-left: 2px; padding-right: 2px; padding-top: 2px"></div>
                    <table id="orderList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>                                <th colspan="6"> <center>Order Information<center></th>
                                <th colspan="8"> <center>Actions<center></th>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Order Confirmation</th>
                                <th>Ref #</th>
                                <th>Client</th>
                                <th>First Delivery</th>
                                <th>Amount</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customer-index')
<script>
$.ajax({
	url: "api/orders/all",
	type: 'GET',
	success: function(result){
	var myObj = $.parseJSON(result);
			$.each(myObj, function(key,value) {
				var t = $('#orderList').DataTable();

				t.row.add( [
							value.id,
							"<a href='<?php echo URL::to('order').'/';?>"+value.id+"'>"+value.order_confirmnation+"</a>",
							value.ref_number,
							value.client,
							value.first_delivery,
							value.amount,
							"<a class='btn btn-small btn-info' href='<?php echo URL::to('order').'/';?>"+value.id+"/edit'><span class='glyphicon glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
							"<form method='POST' action='<?php echo URL::to('order').'/';?>"+value.id+"' accept-charset='UTF-8' class='pull-left' >"+
							"<input name='_method' type='hidden' value='DELETE'>"+
							"<button type='submit' class='btn btn-warning'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>"+"</form>",
					] ).draw();

		});
	}}).error(function(){
			progress.progressTimer('error', {
			errorText:'ERROR!',
			onFinish:function(){
				alert('There was an error processing your information!');
			}
		});
	}).done(function(){
			progress.progressTimer('complete');
			$( "#progressbar" ).fadeOut( "slow" );
	});
</script>
@endsection
