@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> Sign Order</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					<form class="form-horizontal" role="form" method="POST" action="{{ url('sign-accepted/'.$data[0]->sign_link) }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
							<label class="col-md-4 control-label">Order Number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" value="{{$data[0]->order_number}}" />
							</div>
						</div>

            <div class="form-group">
							<label class="col-md-4 control-label">Question</label>
							<div class="col-md-6">
								<textarea rows="7" cols="50" class="form-control">{{$data[0]->question}}</textarea>
							</div>
						</div>

            <div class="form-group">
              <label class="col-md-4 control-label">Rate</label>
              <div class="col-md-6">
                <input type="text" class="form-control" value="{{$data[0]->rate}}" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Currency</label>
              <div class="col-md-6">
                <input type="text" class="form-control" value="{{$data[0]->currency}}" />
              </div>
            </div>

          <center>
						<div class="form-group sigPad">
							<!-- <label class="col-md-6 control-label">Print your name</label> -->
							<!-- <label class="col-md-2 control-label">Your Signature</label> -->
							<div class="">
								<input type="text" name="name" id="name" class="name error form-control">
								<p class="typeItDesc">Review your signature</p>
								<p class="drawItDesc">Draw your signature</p>
								<ul class="sigNav">
									<li class="typeIt"><a href="#type-it" >Type It</a></li>
									<li class="drawIt"><a href="#draw-it" class="current">Draw It</a></li>
									<li class="clearButton"><a href="#clear">Clear</a></li>
								</ul>
								<div class="sig sigWrapper col-md-12">
									<div class="typed"></div>
									<canvas class="pad" width="198" height="80"></canvas>
									<input type="hidden" name="output" class="output">
								</div>
							</div>
						</div>
					</center><br/>



						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('sign-accepted')
<script type="text/javascript">

$(document).ready(function() {
      $('.sigPad').signaturePad();
    });


// $("#orderDeliverySched").change(function() {
//   	if($("#orderDeliverySched").val() == "Daily")
//   	{
//   		$("#orderMainDeliverySched").val("");
//   		$("#orderMainDeliverySched").val("Daily");
//   		$("#divCustomDelivery").css("display", "none");
//   	}
//   	else
//   	{
//   		$("#orderMainDeliverySched").val("");
//   		$("#divCustomDelivery").css("display", "block");
//   	}
// });

// $("#checkDayMon").click(function() {
// 	var value = $("#checkDayMon").val();
// 	$('#orderMainDeliverySched').val(function(i,val) {
//      	return val + (!val ? '' : ',') + value;
// 	});
// });

// $("#checkDayTues").click(function() {
// 	var value = $("#checkDayTues").val();
//   	$('#orderMainDeliverySched').val(function(i,val) {
//      	return val + (!val ? '' : ',') + value;
// 	});
// });

// $("#checkDayWed").click(function() {
// 	var value = $("#checkDayWed").val();
//   	$('#orderMainDeliverySched').val(function(i,val) {
//      	return val + (!val ? '' : ',') + value;
// 	});
// });

// $("#checkDayTh").click(function() {
// 	var value = $("#checkDayTh").val();
//   	$('#orderMainDeliverySched').val(function(i,val) {
//      	return val + (!val ? '' : ',') + value;
// 	});
// });

// $("#checkDayFr").click(function() {
// 	var value = $("#checkDayFr").val();
//   	$('#orderMainDeliverySched').val(function(i,val) {
//      	return val + (!val ? '' : ',') + value;
// 	});
// });

</script>
<script src="{{ asset('js/json2.min.js') }}"></script>
@endsection
