@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Question List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					<div class="loading-progress" id="progressbar" style="padding-left: 2px; padding-right: 2px; padding-top: 2px"></div>
                    <table id="orderQuestionList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            	<th colspan="4"> <center>Question Information<center></th>
                                <th colspan="6"> <center>Actions<center></th>
                            </tr>
                            <tr>
                                <th>Date</th>
                                <th>Question Code</th>
                                <th>Order ID</th>
                                <th>P.O Price</th>
                                <th>View</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('question-index')
<script>
$.ajax({
url: "api/question_h/all",
type: 'GET',
success: function(result){
	var myObj = $.parseJSON(result);
	$.each(myObj, function(key,value) {
				var t = $('#orderQuestionList').DataTable();

				t.row.add( [
							value.date,
							value.question_code,
							value.order_id,
							value.po_price,
							"<a class='btn btn-small btn-info' href='<?php echo URL::to('question').'/';?>"+value.id+"'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a>",
							'',

					] ).draw();

		});
}});
</script>
@endsection
