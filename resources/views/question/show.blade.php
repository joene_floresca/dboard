@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">Question Information</div>
				<div class="panel-body">
					

					<div class="form-horizontal">
						
						<div class="form-group">
							<label class="col-md-4 control-label">ID</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->id }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Date</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->date }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Question Code</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->question_code }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">PO Price</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->po_price }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Order ID</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->order_id }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">MIS Status</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->mis_status }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">QA Status</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->qa_status }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Prod Status</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->prod_status }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Age</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->age }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Homeowner</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->is_homeowner }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Covered Areas</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->covered_areas }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Postcode Exclusion</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->postcode_exclusion }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Telephone Type</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->telephone_type }}</label>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Others</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->others }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">System Date Created</label>
							<div class="col-md-6">
								<label class="col-md-6 control-label">{{ $headerData->created_at }}</label>
							</div>
						</div>

						<table class="table table-striped table-bordered">
						    <thead>
						        <tr>
						            <td class="bg-info">Question Header ID</td>
						            <td class="bg-info">Question</td>
						            <td class="bg-info">Response</td>
						            <td class="bg-info">Created at</td>
						        </tr>
						    </thead>
						    <tbody>
						    @foreach($headerItems as $key => $value)
						        <tr>
						            <td class="bg-warning">{{ $value->question_headers_id }}</td>
						            <td class="bg-warning">{{ $value->question }}</td>
						            <td class="bg-warning">{{ $value->response }}</td>
						            <td class="bg-warning">{{ $value->created_at }}</td>
						        </tr>
						    @endforeach
						    </tbody>
						</table>

					</div>	
						
				</div>
			</div>

			

			


		</div>
	</div>
</div>
@endsection
